package test;

import org.json.JSONObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.Date;

/**
 * Created by admin on 29/7/16.
 */
public class TestGetNextScript {

    public static void main(String arg[]) throws Exception {
        //getNextImage();
        getNextImageOnThs();
        getNextImageOnThs();
        getNextImageOnThs();
        getNextImageOnThs();
        getNextImageOnThs();
    }

    public static void getNextImage(){
        NextImage nextImage = new NextImage("27");
        nextImage.getNextMobile();
    }

    public static void getNextImageOnThs(){

        Thread th1 = new Thread(new NextImage("30"));
        th1.start();

        Thread th2 = new Thread(new NextImage(("28")));
        th2.start();

        Thread th3 = new Thread(new NextImage(("29")));
        th3.start();

        Thread th4 = new Thread(new NextImage(("31")));
        th4.start();

        Thread th5 = new Thread(new NextImage(("32")));
        th5.start();
    }
}


class NextImage implements Runnable {

    private String userId = null;

    public NextImage(String userId) {
        this.userId = userId;
    }

    @Override
    public void run() {
        for (int i = 0; i <= 100; i++) {
            System.out.println("UserId :  " + userId + "   loading ..... " + i);
            JSONObject msg = getNextMobile();
            if (msg.get("RES_STATUS").equals("F")) {
                break;
            }
        }
    }


    public JSONObject getNextMobile() {
        JSONObject msg = new JSONObject();
        //String restUrl = "http://52.20.55.190:8081/get_next_mobile";
        String restUrl = "http://localhost:8081/get_next_clips_s3";

        MultivaluedMap<String, Object> headerMap = new MultivaluedHashMap<String, Object>();
        long idTime = new Date().getTime();

        msg.put("NO_OF_CLIPS", "3");
        msg.put("SQS_NO", "2");

        long time1= new Date().getTime();

        System.out.println("RQ TO API-SERVER  : "  + msg.toString());
        Client client = ClientBuilder.newClient();
        JSONObject res = new JSONObject(client.target(restUrl).request(MediaType.APPLICATION_JSON_TYPE).
                headers(headerMap).post(Entity.entity(msg.toString(), MediaType.APPLICATION_JSON_TYPE), String.class));

        System.out.println("RESPONSE FROM API-SERVER  : " + "ID-" + idTime + " : " + res.toString());
        System.out.println("RESPONSE TIME : " + ( new Date().getTime() - time1 ));
        return res;
    }

}