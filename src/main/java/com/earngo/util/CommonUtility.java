package com.earngo.util;

import org.json.JSONArray;

/**
 * Created by Rajesh on 15/7/16.
 */
public class CommonUtility {

    public static String nullValue() {
        return null;
    }


    public static String emptyString() {
        return "";
    }

    public static JSONArray emptyJSONArray(){
        return new JSONArray();
    }

    public static void main(String[] a) {
        System.out.println(nullValue());
    }
}
