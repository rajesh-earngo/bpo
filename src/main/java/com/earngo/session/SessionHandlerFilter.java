package com.earngo.session;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created by admin on 30/7/16.
 */
public class SessionHandlerFilter extends HttpServlet {

    private static final String NAME = "name";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        // get name from session, or return default

        String name = req.getSession(false).getAttribute(NAME).toString();
        // create a greeting with the name
        String greeting = String.format("Hello %s!", name);

        // write response
        try (ServletOutputStream out = resp.getOutputStream()) {
            out.write(greeting.getBytes(StandardCharsets.UTF_8));
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        // get the name parameter from the request (possibly null)
        String name = req.getParameter(NAME);
        // store the name variable as a session attribute
        req.getSession().setAttribute(NAME, name);
    }
}
