package com.earngo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Rajesh on 6/12/16.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class UnknownFailureError extends RuntimeException {

    public UnknownFailureError() {
    }

    public UnknownFailureError(String message) {
        super(message);
    }

    public UnknownFailureError(String message, Throwable cause) {
        super(message, cause);
    }
}
