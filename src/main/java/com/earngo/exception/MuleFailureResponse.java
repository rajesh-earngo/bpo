package com.earngo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by rajeshkumarb on 05/07/2016.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class MuleFailureResponse extends RuntimeException {

    public MuleFailureResponse() {
    }

    public MuleFailureResponse(String message) {
        super(message);
    }

    public MuleFailureResponse(String message, Throwable cause) {
        super(message, cause);
    }
}
