package com.earngo.service.quality;

import com.earngo.service.BaseRestService;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by Rajesh on 10/7/16.
 */
@Service
public class QualityService extends BaseRestService {

    public String fetchAllForms(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "FETCH_ALL_QUALITY_FORMS");
    }

    public String fetchFormImages(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "FETCH_FORM_IMAGES");
    }

    public String fetchImagesData(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "FETCH_IMAGE_DATA");
    }

}
