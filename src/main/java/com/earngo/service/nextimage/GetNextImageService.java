package com.earngo.service.nextimage;

import com.earngo.util.EarngoConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;

@Service
public class GetNextImageService extends AbstractNextImage {

    /**
     * Post the result to Earngo
     *
     * @param headerMap
     * @param uiJsonStr
     * @return
     */
    public JSONObject postResultToEarngo(MultivaluedMap<String, Object> headerMap, String uiJsonStr) {
        JSONArray uiJsonArr = new JSONArray(uiJsonStr);
        String response = null;
        for (int i = 0; i < uiJsonArr.length(); i++) {
            JSONObject muleJsonReq = createJsonRequestObj(uiJsonArr.getJSONObject(i));
            response = restCall(headerMap, muleJsonReq.toString(), "POST_INPUT_MOBILE");
        }
        return new JSONObject(response);
    }


    /**
     * Post the result array to Earngo
     */
    public JSONObject postResultArrayToEarngo(MultivaluedMap<String, Object> headerMap, String jsonStr) {
        JSONArray jsonArr = new JSONArray(jsonStr);
        JSONObject jsonRes = createJsonRequestArrayObj(jsonArr);
        String response  = restCall(headerMap, jsonRes.toString(), "POST_INPUT_ARRAY");
        return new JSONObject(response);
    }


    private JSONObject createJsonRequestObj(JSONObject obj) {
        String clipValue = obj.get("clipValue").toString();
        JSONObject muleReqJson = new JSONObject();
        muleReqJson.put("DISTRIBUTOR_NUM", obj.get("clipNo"));
        muleReqJson.put("OUTTRAY_INPUT", obj.get("clipValue"));
        if (clipValue.equals(EarngoConstants.UNCLEAR_VALUE)) {
            // action is 2 when unclear image is selected
            muleReqJson.put("OUTTRAY_ACTION", EarngoConstants.OUT_TRAY_ACTION_UNCLEAR);
        } else {
            muleReqJson.put("OUTTRAY_ACTION", EarngoConstants.OUT_TRAY_ACTION_VALUE);
        }
        return muleReqJson;
    }


    private JSONObject createJsonRequestArrayObj(JSONArray jsonArr) {
        JSONArray newJsonArr = new JSONArray();
        for (int i = 0; i < jsonArr.length(); i++) {
            JSONObject obj = jsonArr.getJSONObject(i);
            newJsonArr.put(createJsonRequestObj(obj));
        }
        JSONObject obj = new JSONObject();
        obj.put("array", newJsonArr);
        return obj;
    }
}
