package com.earngo.service.nextimage;

import com.earngo.util.CommonUtility;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

@Service
public class CheckerGetNextImageService extends AbstractNextImage {

    /**
     * Fetch Checker images
     *
     * @param headerMap
     * @param noOfClips
     * @param isPageLoad
     * @return
     */
    public JSONObject getCheckerImagesFromEarngo(MultivaluedMap<String, Object> headerMap, int noOfClips, boolean isPageLoad) {

        JSONObject muleResponseObj = getImagesFromEarngo(headerMap,null,  noOfClips, isPageLoad);

        JSONArray listOfObj1 = muleResponseObj.getJSONArray("imagesSetOne");
        JSONArray listOfObj2 = muleResponseObj.getJSONArray("imagesSetTwo");

        populateCheckerData(headerMap, listOfObj1);
        populateCheckerData(headerMap, listOfObj2);
        return muleResponseObj;
    }


    /**
     * Fetch checker Large images
     *
     * @return
     */
    public JSONObject getCheckerLargeImagesFromEarngo(MultivaluedMap<String, Object> headerMap, String jsonStr, int noOfClips, boolean isPageLoad) {

        JSONObject muleResponseObj = getLargeImagesFromEarngo(headerMap, jsonStr,noOfClips, isPageLoad);
        JSONArray listOfObj1 = muleResponseObj.getJSONArray("imagesSetOne");
        JSONArray listOfObj2 = muleResponseObj.getJSONArray("imagesSetTwo");
        populateCheckerData(headerMap, listOfObj1);
        populateCheckerData(headerMap, listOfObj2);
        return muleResponseObj;
    }


    private String createJsonResObj(JSONObject obj) {

        JSONObject objRes = new JSONObject();
        objRes.put("DISTRIBUTOR_NUM", obj.get("clipNo"));
        objRes.put("OUTTRAY_ACTION", obj.get("clipOuttrayAction"));
        //NEED TO CHECK WHAT IS THIS VALUE
        objRes.put("OUTTRAY_INPUT", obj.get("clipValue"));
        return objRes.toString();
    }


    public JSONObject postResultToEarngo(MultivaluedMap<String, Object> headerMap, String jsonStr) {
        JSONArray jsonArr = new JSONArray(jsonStr);
        String response = null;
        for (int i = 0; i < jsonArr.length(); i++) {
            JSONObject obj = jsonArr.getJSONObject(i);
            String jsonRes = createJsonResObj(obj);
            response = restCall(headerMap, jsonRes.toString(), "POST_DATA_CHECKER");

        }
        return new JSONObject(response);
    }

    private void populateCheckerData(MultivaluedMap<String, Object> headerMap, JSONArray listOfClips) {
        for (int i = 0; i < listOfClips.length(); i++) {
            JSONObject clipObj = listOfClips.getJSONObject(i);
            String checkerData = getCheckerDataForImage(clipObj.get("clipNo").toString(), headerMap);
            JSONObject checkerResponseJson = new JSONObject(checkerData);
            String status = checkerResponseJson.getString("RES_STATUS");
            if (status == null || status.equalsIgnoreCase("F") == true) {
                clipObj.put("checkerDataArr", CommonUtility.emptyString());
            } else {
                clipObj.put("checkerDataArr", checkerResponseJson.getJSONArray("DATAARRAY"));
            }
        }
    }

    /**
     * It extracts data for each image  with Distribution no
     *
     * @param distributorNum
     * @param headerMap
     * @return
     */
    private String getCheckerDataForImage(String distributorNum, MultivaluedMap<String, Object> headerMap) {

        JSONObject muleReqJson = new JSONObject();
        muleReqJson.put("DISTRIBUTOR_NUM", distributorNum);
        String response = restCall(headerMap, muleReqJson.toString(), "GET_DATA_CHECKER");

        int value = response.indexOf("RES_STATUS");
        if (value < 0) {
            JSONObject obj = new JSONObject();
            obj.put("RES_STATUS", "S");
            obj.put("checker_data", response);
            return obj.toString();
        }
        return response;
    }
}
