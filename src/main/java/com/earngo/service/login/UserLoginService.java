package com.earngo.service.login;

import com.earngo.service.BaseRestService;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;

@Service
public class UserLoginService extends BaseRestService {


    public String validateUserLogin(MultivaluedMap<String, Object> headerMap, String uiJsonStr) {
        JSONObject uiJsonObj = new JSONObject(uiJsonStr);
        JSONObject muleReqObj = new JSONObject();
        muleReqObj.put("WRK_EMAIL", uiJsonObj.get("username"));
        muleReqObj.put("WRK_PASSWORD", uiJsonObj.get("password"));
        return restCall(headerMap, muleReqObj.toString(), "VALIDATE_USER");
    }

    public String getActiveTaskForBPO(MultivaluedMap<String, Object> headerMap, String jsonStr) {
        return restCall(headerMap, jsonStr.toString(), "ACTIVE_TASK_BPO");
    }


    public String initializeDevice(MultivaluedMap<String, Object> headerMap, String jsonStr) {
        headerMap.add("deviceId", 0);
        return restCall(headerMap, jsonStr.toString(), "INIT_DEVICE");
    }

}
