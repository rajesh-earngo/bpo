package com.earngo.service;

import com.earngo.configuration.AppProperties;
import com.earngo.exception.ConnectionFailure;
import com.earngo.exception.MuleFailureResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.Date;

/**
 * Created by Rajesh on 21/5/16.
 */
public abstract class BaseRestService {

    private Logger logger = Logger.getLogger(BaseRestService.class);
    @Autowired
    protected AppProperties appProp;


    protected String callToEarngo(String restUrl, MultivaluedMap<String, Object> headerMap, String msg) throws Exception {

        headerMap.add("CUST_SRNO_TYPE", "P");
        headerMap.add("WRK_LANGUAGE_1", "1"); // need to modify in future
        long idTime = new Date().getTime();

        logger.info("REST CALL TO API-SERVER : " + "ID-" + idTime + " : " + restUrl + "  MSG BODY : " + msg + " HEADER : " + new JSONObject(headerMap).toString());

        Client client = ClientBuilder.newClient();
        String res = client.target(restUrl).request(MediaType.APPLICATION_JSON_TYPE).headers(headerMap)
                        .post(Entity.entity(msg.toString(), MediaType.APPLICATION_JSON_TYPE), String.class);

        logger.info("RESPONSE FROM API-SERVER  : " + "ID-" + idTime + " : " + res);
        return res;

    }


    public String getMuleRestURL(String prop) {
        return appProp.getAppUrl() + ":" + appProp.getProperty(prop);
    }


    protected String restCall(MultivaluedMap<String, Object> headerMap, String uiReqJson, String urlProp) {
        String response = null;
        try {
            response = callToEarngo(getMuleRestURL(urlProp), headerMap, uiReqJson.toString());
        } catch (Exception e) {
            logger.error("Error in Mule Server Call", e);
            throw new ConnectionFailure("API server Connection failure. Please contact support team.");
        }


        if(new JSONObject(response).get("RES_STATUS").equals("E")){
            throw new MuleFailureResponse("Error in API server. Please contact support team.");
        }
        return response;

    }
}
