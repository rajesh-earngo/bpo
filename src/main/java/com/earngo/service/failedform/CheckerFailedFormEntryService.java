package com.earngo.service.failedform;

import com.earngo.util.EarngoConstants;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.io.File;

public class CheckerFailedFormEntryService{
	/*

		
	public String getAllFailedImageListName(MultivaluedMap<String, Object> headerMap,
			String jsonStr) {
		return null;
	}
	
	

	public String getImageDetails(MultivaluedMap<String, Object> headerMap,
			String jsonStr) {
 		
		JSONObject reqObj = new JSONObject();
		reqObj.put("CUST_SRNO_TYPE", EarngoConstants.CUST_SRNO_TYPE);
		reqObj.put("WRK_LANGUAGE_1", EarngoConstants. WRK_LANGUAGE_1);
		reqObj.put("NO_OF_CLIPS", 1);
		
		GeneralRequestService restService = new GeneralRequestService();
		String response = null;
		try {
			response = restService.restClientToEarngo( EarngoConstants.GET_CHECKER_NEXT_EXCEPTION_IMAGE, headerMap, reqObj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Need to handle when res is F later
		return parseResponseOfFaileImageDetails(response.toString());	 
	}
	
	

	public String saveFailedFormData(MultivaluedMap<String, Object> headerMap,
			String jsonStr) {
		
		JSONObject obj = new JSONObject();
		String earngoId= ""+headerMap.get("EARNGO_ID");
		
		String reqStr= parseJsObjIntoMulePostResObj(jsonStr,earngoId); 

		System.out.println("Request Data ==  "+ obj.toString());
		GeneralRequestService restService = new GeneralRequestService();
		String response = null;
		try {
			response = restService.restClientToEarngo( EarngoConstants.POST_CHECKER_NEXT_EXCEPTION_IMAGE, headerMap, reqStr.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} 

		return response.toString();
	}
	

	private String parseResponseOfFaileImageDetails(String jsonStr){
		
		JSONObject jsonObj=new JSONObject(jsonStr);
		
		JSONObject obj = new JSONObject();
		String status=""+jsonObj.get("RES_STATUS");
		 
		obj.put("RES_STATUS", jsonObj.get("RES_STATUS"));
		obj.put("RES_DESC", jsonObj.get("RES_MESSAGE"));
		obj.put("RES_CODE", jsonObj.get("RES_CODE"));
		
		if(status.equals("F")== true){
			return obj.toString();
		}
		
		
		String imagePath=null;
		String imageNo=null;
		 
		JSONArray arr=new JSONArray();
		JSONArray resArr=jsonObj.getJSONArray("DATAARRAY");
		  
		for(int i=0;i<resArr.length();i++){
			
			
			JSONObject resObj = new JSONObject(""+resArr.get(i));
			 
			JSONObject o1 = new JSONObject();
			o1.put("clipNo",resObj.get("CUTTER_CLIP_NUM"));   
			o1.put("imgNo",resObj.get("IMG_NUM"));
			
			if(resObj.get("CUTTER_CLIP_NAME") ==null || resObj.get("CUTTER_CLIP_NAME").equals("null")){
				o1.put("clipName","Label - NA");
			}else{
				o1.put("clipName",resObj.get("CUTTER_CLIP_NAME"));
			} 
			o1.put("originClipType", resObj.get("CUTTER_CLIP_TYPE"));
			
			//Note- Clip type here modified 
			String clipType=""+resObj.get("CUTTER_CLIP_TYPE");
			
			if(clipType.equals("ST")==true || clipType.equals("LT")==true  || clipType.equals("DT")==true  || clipType.equals("NM")==true
					 || clipType.equals("TM")==true  || clipType.equals("DM")==true){
			 //do nothing	
			}else if (clipType.equals("XL")==true || clipType.equals("XLMMV")==true){
				clipType="LT";
			}
			else{
				clipType="ST";
			}
				
			o1.put("clipType",clipType); 
			o1.put("CUSTOM_MESSAGE", resObj.get("CUSTOM_MESSAGE"));
			
			if(i==0){
				imageNo=""+resObj.get("IMG_NUM");
			}
			
			o1.put("outtray_action", resObj.get("OUTTRAY_ACTION"));
			o1.put("clipValue", resObj.get("OUTTRAY_INPUT")); 
			o1.put("imgPath", jsonObj.get("IMAGE_URL"));
			o1.put("cust_srno", jsonObj.get("CUST_SRNO"));
			
			arr.put(o1);
		} 
		
		//obj.put("image_path", imagePath);
		//obj.put("image_id", imageNo);

		String path = createImagePath(jsonObj.get("IMAGE_URL").toString() ,jsonObj.get("CUST_SRNO").toString()) ;
		obj.put("custFormPath", path);
		obj.put("custFormId", imageNo);

		//put the array
		obj.put("fieldsArray", arr);
	
		System.out.println("Values from Checker : "+obj.toString());
		
		return obj.toString();
		
	}


	private String createImagePath(String path, String custoNo) {
		String custFormPath = EarngoConstants.EARNGO_UNKNOWWN_IMAGES_URL + custoNo + path.substring(path.lastIndexOf(File.separator));
		return custFormPath;
	}

	private String parseJsObjIntoMulePostResObj(String jsonStrJS,String earngoId){
		  
		JSONArray jsObjArr=new JSONArray(jsonStrJS);
		
		JSONObject muleObj=null;
		JSONArray muleObjArr=new JSONArray(); 
 		
		for(int i=0;i<jsObjArr.length();i++){
			
			JSONObject jsObj = new JSONObject(""+jsObjArr.get(i));
			
			muleObj=new JSONObject();
			muleObj.put("CLIP_ID", jsObj.get("clipNo"));
			muleObj.put("CLIP_TYPE", jsObj.get("originClipType"));
			muleObj.put("CLIP_NAME", jsObj.get("clipName"));
			muleObj.put("IMG_NUM", jsObj.get("imgNo"));
			muleObj.put("OUTTRAY_INPUT", jsObj.get("clipValue")); 
			muleObj.put("IMAGE_PATH", jsObj.get("imgPath"));  
			muleObj.put("CUSTOM_MESSAGE", ""); 
			
			String clipValue=""+jsObj.get("clipValue");
			if(clipValue.equals("*")){
				// action is 2 when unclear image is selected
				muleObj.put("OUTTRAY_ACTION", 2);
			}else{
				muleObj.put("OUTTRAY_ACTION", 1);
			} 
			muleObjArr.put(muleObj);
		} 
		JSONObject newObj=new JSONObject();
		newObj.put("IMAGE_DATA", muleObjArr);
		
		return newObj.toString();
	}
	

	public static void main(String[] args) {
		
		
		System.out.println("Calling Rest client to EarngoApp");
		 
		Client client = ClientBuilder.newClient();

		JSONObject jsonReqObj = new JSONObject();
		jsonReqObj.put("CUST_SRNO_TYPE", "P");
		jsonReqObj.put("WRK_LANGUAGE_1", "1"); 
		jsonReqObj.put("CUST_SRNO", "1");

		MultivaluedMap<String, Object> headerMap = new MultivaluedHashMap<String, Object>();
		headerMap
				.add("EARNGO_DEVICEID", "d6d856d6-b382-11e4-bf6f-433dd1e0a480");
		headerMap.add("EARNGO_ID", "1");
		headerMap.add("EARNGO_IP", "/54.225.115.94:39189");
		headerMap.add("EARNGO_SESSION_VALUE", "d6d856d6-b382-11e4-bf6f-433dd1e0a480");

		//new FailedFormEntryService().getNextMatchFormDetails(headerMap, jsonReqObj.toString());
		
		//System.out.println("Res :" + res);


		//System.out.println(new FailedFormEntryService().getNextMatchFormDetails(null,null));
	}
	*/

}
