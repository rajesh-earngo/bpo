package com.earngo.service.failedform;

import com.earngo.service.BaseRestService;
import com.earngo.util.EarngoConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;
import java.io.File;

@Service
public class FailedFormEntryService extends BaseRestService {

    /**
     * Get the image path, its all fields details
     *
     * @param headerMap
     * @param jsonStr
     * @return
     */
    public String getImageDetails(MultivaluedMap<String, Object> headerMap, String jsonStr) {

        JSONObject uiReqJson = new JSONObject();
        uiReqJson.put("CUST_SRNO_TYPE", EarngoConstants.CUST_SRNO_TYPE);
        uiReqJson.put("WRK_LANGUAGE_1", EarngoConstants.WRK_LANGUAGE_1);
        uiReqJson.put("NO_OF_CLIPS", 1);

        String response = restCall(headerMap, uiReqJson.toString(), "GET_EXCEPTION_IMAGE_DETAIL");
        //Need to handle when res is F later
        return parseResponseOfFaileImageDetails(response.toString());
    }


    /**
     * Get the image path, its all fields details
     *
     * @param headerMap
     * @param jsonStr
     * @return
     */
    public String saveFailedFormData(MultivaluedMap<String, Object> headerMap, String jsonStr) {
        String reqStr = parseJsObjIntoMulePostResObj(jsonStr);
        return restCall(headerMap, reqStr, "POST_EXCEPTION_IMAGE_DETAIL_DATA");
    }


    /**
     * It retrieves details for Single failed Image
     *
     * @param headerMap
     * @param jsonStr
     * @return
     */
    public String getNextMatchFormDetails(MultivaluedMap<String, Object> headerMap, String jsonStr) {


        JSONObject reqObj = new JSONObject();
        reqObj.put("CUST_SRNO_TYPE", EarngoConstants.CUST_SRNO_TYPE);
        reqObj.put("WRK_LANGUAGE_1", EarngoConstants.WRK_LANGUAGE_1);
        reqObj.put("NO_OF_CLIPS", 1);

        String response = restCall(headerMap, reqObj.toString(), "GET_MATCH_EXCEPTION");

        JSONObject resObj = new JSONObject(response);

        if (resObj.get("RES_STATUS").toString().equals("F") == true) {
            return response;
        }

        JSONObject obj = new JSONObject();
        obj.put("cust_srno", resObj.get("cust_srno"));
        obj.put("unknown_image_id", resObj.get("unknown_image_id"));
        obj.put("SEQ_NO", resObj.get("SEQ_NO"));
        obj.put("RES_STATUS", resObj.get("RES_STATUS"));

        String path = createImagePath(resObj.get("unknown_image_path").toString(), resObj.get("cust_srno").toString());
        obj.put("unknown_image_path", path);

        JSONArray arr = resObj.getJSONArray("templateArray");
        JSONArray arr2 = new JSONArray();
        for (int i = 0; i < arr.length(); i++) {
            JSONObject f = new JSONObject();
            f.put("template_id", arr.getJSONObject(i).get("template_id"));
            f.put("template_path", EarngoConstants.EARNGO_CUSTOMER_URL + "" + arr.getJSONObject(i).get("template_path"));
            f.put("form_id", arr.getJSONObject(i).get("form_id"));
            f.put("sub_form_id", arr.getJSONObject(i).get("sub_form_id"));
            f.put("template_name", arr.getJSONObject(i).get("template_name"));
            f.put("cust_name", arr.getJSONObject(i).get("cust_name"));
            arr2.put(f);
        }
        obj.put("templateArray", arr2);
        return obj.toString();
    }


    /**
     * Get the image path, its all fields details
     *
     * @param headerMap
     * @param jsonStr
     * @return
     */
    public String saveMatchFormDetails(MultivaluedMap<String, Object> headerMap, String jsonStr) {
        JSONObject jsonObj = new JSONObject(jsonStr);
        JSONObject reqObj = new JSONObject();
        reqObj.put("CUST_SRNO", "" + jsonObj.get("custNo"));
        reqObj.put("UNKNOWN_IMAGE_ID", jsonObj.get("failedImageId"));
        reqObj.put("TEMPLATE_ID", jsonObj.get("failedImageLinkTemplate"));
        reqObj.put("TEMPLATE_NAME", jsonObj.get("failedImageLinkTemplateName"));
        reqObj.put("IS_IMAGE_MATCHED", jsonObj.get("isImageMatch"));
        reqObj.put("SEQ_NO", jsonObj.get("seqNo"));
        return restCall(headerMap, reqObj.toString(), "POST_MATCH_EXCEPTION");
    }


    /**
     * Here Map between Java script and mule fields
     *
     * @param jsonStr
     * @return
     */
    private String parseResponseOfFaileImageDetails(String jsonStr) {

        JSONObject jsonObj = new JSONObject(jsonStr);

        JSONObject obj = new JSONObject();
        String status = "" + jsonObj.get("RES_STATUS");

        obj.put("RES_STATUS", jsonObj.get("RES_STATUS"));
        obj.put("RES_DESC", jsonObj.get("RES_MESSAGE"));
        obj.put("RES_CODE", jsonObj.get("RES_CODE"));

        if (status.equals("F") == true) {
            return obj.toString();
        }

        JSONArray arr = new JSONArray();
        JSONArray resArr = jsonObj.getJSONArray("CLIPS");

        for (int i = 0; i < resArr.length(); i++) {

            JSONObject resObj = new JSONObject(resArr.get(i).toString());

            JSONObject o1 = new JSONObject();
            o1.put("clipNo", resObj.get("CLIP_ID"));

            if (resObj.get("CLIP_NAME") == null || resObj.get("CLIP_NAME").equals("null")) {
                o1.put("clipName", "Label - NA");
            } else {
                o1.put("clipName", resObj.get("CLIP_NAME"));
            }
            o1.put("clipType", resObj.get("CLIP_TYPE"));
            o1.put("customMessage", resObj.get("CUSTOM_MESSAGE"));
            o1.put("imgPath", jsonObj.get("IMAGE_URL"));
            o1.put("imgNo", jsonObj.get("IMAGE_ID"));

            arr.put(o1);
        }

        String path = createImagePath(jsonObj.get("IMAGE_URL").toString(), jsonObj.get("CUST_SRNO").toString());
        obj.put("custFormPath", path);
        obj.put("custFormId", jsonObj.get("IMAGE_ID"));

        //put the array
        obj.put("fieldsArray", arr);

        return obj.toString();

    }


    private String parseJsObjIntoMulePostResObj(String jsonStrJS) {

        JSONArray jsObjArr = new JSONArray(jsonStrJS);

        JSONObject muleObj = null;
        JSONArray muleObjArr = new JSONArray();

        for (int i = 0; i < jsObjArr.length(); i++) {

            JSONObject jsObj = new JSONObject("" + jsObjArr.get(i));

            muleObj = new JSONObject();
            muleObj.put("IMG_NUM", jsObj.get("imgNo"));
            muleObj.put("CLIP_NAME", jsObj.get("clipName"));
            muleObj.put("CLIP_TYPE", jsObj.get("clipType"));
            muleObj.put("OUTTRAY_INPUT", jsObj.get("clipValue"));
            muleObj.put("CLIP_ID", jsObj.get("clipNo"));
            muleObj.put("IMAGE_PATH", jsObj.get("imgPath"));

            String clipValue = "" + jsObj.get("clipValue");
            if (clipValue.equals("*")) {
                // action is 2 when unclear image is selected
                muleObj.put("OUTTRAY_ACTION", 2);
            } else {
                muleObj.put("OUTTRAY_ACTION", 1);
            }
            muleObjArr.put(muleObj);
        }

        JSONObject newObj = new JSONObject();
        newObj.put("IMAGE_DATA", muleObjArr);

        return newObj.toString();
    }

    private String createImagePath(String path, String custoNo) {

        int index = path.lastIndexOf("\\");
        if (index == -1) {
            index = path.lastIndexOf(File.separator);
        }
        if (index == -1) {
            index = path.lastIndexOf("/");
        }
        String custFormPath = EarngoConstants.EARNGO_UNKNOWWN_IMAGES_URL + custoNo + path.substring(index);
        return custFormPath;
    }
}
