package com.earngo.service.forms;

import com.earngo.configuration.AppProperties;
import com.earngo.util.ResponseMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Rajesh on 17/5/16.
 */
@PropertySource(value = {"classpath:config/application.properties"})
@Service
public class FormsUploadService {

    @Autowired
    private AppProperties appProp;

    public ResponseMsg uploadMultipleFileHandler(MultipartFile[] files) {

        if (files == null && files.length == 0) {
            return new ResponseMsg("F", "Empty files.", null);
        }

         List<ResponseMsg> list = new ArrayList<ResponseMsg>();
        Map<String, String> filesStatus = new HashMap<String, String>();
        String status = "S";
        String desc = "Uploaded Successfully";
        ResponseMsg msg = null;
        for (MultipartFile file : files) {
            msg = uploadFileHandler(file);
            filesStatus.put(file.getOriginalFilename(), msg.getStatus());
            if (msg.getStatus().equals("F")) {
                status = "F";
                desc = "Error in Server";
            }
        }
        return new ResponseMsg(status, desc, null).setData(filesStatus);
    }

    public ResponseMsg uploadFileHandler(MultipartFile file) {
        byte[] bytes;
        try {
            if (!file.isEmpty()) {
                String uploadPath= appProp.getTemplateUploadPath()+ file.getOriginalFilename();
                bytes = file.getBytes();
                //store file in storage
                Path path = Paths.get(uploadPath);
                Files.write(path, bytes); //creates, overwrites
            } else {
                return new ResponseMsg("F", "Empty file.", file.getOriginalFilename());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return new ResponseMsg("F", "Error in Server :" + file.getOriginalFilename(), ex.getMessage());
        }
        return new ResponseMsg("S", "Uploaded Successfully", file.getOriginalFilename());
    }

}
