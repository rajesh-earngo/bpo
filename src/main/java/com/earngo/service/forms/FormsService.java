package com.earngo.service.forms;

import com.earngo.service.BaseRestService;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;
import java.io.File;
import java.util.*;

/**
 * Created by Rajesh on 17/5/16.
 */
@Service
public class FormsService extends BaseRestService {


    //private Logger logger = Logger.getLogger(FormsService.class);

    public String validateTemplate(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "VALIDATE_TEMPLATE");
    }


    public String registerTemplate(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        JSONObject uiReq = new JSONObject(uiReqJson);
        uiReq.put("IMAGE_NAME",appProp.getProperty("templates.upload.location")+ uiReq.get("IMAGE_NAME"));
        return restCall(headerMap, uiReq.toString(), "REGISTER_TEMPLATE");
    }


    public String fetchFormsDetails(MultivaluedMap<String, Object> headerMap, String uiReqJson) {

        JSONObject response = new JSONObject();
        return transformForUI(restCall(headerMap, uiReqJson, "FETCH_TEMPLATE"));
    }

    private JSONArray getAllCategory(String muleRes) {
        JSONObject muleJson = new JSONObject(muleRes);
        JSONArray formDetails = muleJson.getJSONArray("FORM_DETAILS");

        List<JSONObject> categoryList = new ArrayList<>();

        HashMap<String, String> map = new HashMap<>();

        for (int i = 0; i < formDetails.length(); i++) {
            JSONObject category = new JSONObject();
            category.put("categoryId", formDetails.getJSONObject(i).getInt("CATEGORY_ID"));
            category.put("categoryName", formDetails.getJSONObject(i).getString("CATEGORY_NAME"));

            if (i > 0 && formDetails.getJSONObject(i).getInt("CATEGORY_ID") ==
                    categoryList.get(categoryList.size() - 1).getInt("categoryId")) {
                //do not do anything
            } else {
                //map.put()
                categoryList.add(category);
            }
        }
        return new JSONArray(categoryList.toArray());
    }


    private String transformForUI(String muleRes) {

        JSONObject muleJson = new JSONObject(muleRes);
        if(muleJson.get("RES_STATUS").equals("F")){
            return muleRes;
        }
        JSONArray formDetails = muleJson.getJSONArray("FORM_DETAILS");

        JSONArray pages = new JSONArray();
        JSONObject page = null;

        JSONArray forms = new JSONArray();
        JSONObject form = new JSONObject();

        JSONArray categories = new JSONArray();
        JSONObject category = new JSONObject();

        JSONObject cat = null;
        JSONObject tempCat = null;

        for (int i = 0; i < formDetails.length(); i++) {
            page = new JSONObject();

            if (i != 0) {
                tempCat = formDetails.getJSONObject(i - 1);
            }
            cat = formDetails.getJSONObject(i);

            //form id changed
            if (tempCat != null && tempCat.getInt("FORM_ID") == cat.getInt("FORM_ID")) {
            } else if (tempCat != null) {
                forms.put(form);
                pages = new JSONArray();
                form = new JSONObject();
            }

            //category Id changed
            if (tempCat != null && tempCat.getInt("CATEGORY_ID") == cat.getInt("CATEGORY_ID")) {
            } else if (tempCat != null) {
                categories.put(category);
                category = new JSONObject();
                pages = new JSONArray();
                form = new JSONObject();
                forms = new JSONArray();
            }

            page.put("pageId", cat.getInt("TEMPLATE_ID"));
            page.put("pageName", cat.getString("TEMPLATE_NAME"));
            page.put("pageUrl", cat.getString("NORMALISED_PATH"));

            form.put("formId", cat.getInt("FORM_ID"));
            form.put("formName", cat.getString("FORM_NAME"));

            pages.put(page);
            form.put("pages", pages);

            category.put("categoryId", cat.getInt("CATEGORY_ID"));
            category.put("categoryName", cat.getString("CATEGORY_NAME"));
            category.put("forms", forms);

            if (i == formDetails.length() - 1) {
                //last element
                forms.put(form);
                categories.put(category);
            }
        }

        //create response
        JSONObject uiResponse = new JSONObject();
        uiResponse.put("CUST_SRNO", muleJson.getInt("CUST_SRNO"));
        uiResponse.put("RES_STATUS", muleJson.getString("RES_STATUS"));
        uiResponse.put("RES_CODE", muleJson.getString("RES_CODE"));
        uiResponse.put("RES_MESSAGE", muleJson.getString("RES_MESSAGE"));
        uiResponse.put("FORM_DETAILS", categories);
        uiResponse.put("CATEGORY_DETAILS", getAllCategory(muleRes));

        return uiResponse.toString();
    }
}
