package com.earngo.service.profile;


import com.earngo.service.BaseRestService;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by admin on 30/7/16.
 */

@Service
public class UserProfileService extends BaseRestService {

    public String getProfile(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "FETCH_USER_PROFILE");
    }

    public String updateProfile(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "UPDATE_USER_PROFILE");
    }

    public String redeemPoints(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "REDEEM_USER_POINTS");
    }

    public String fetchPoints(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "FETCH_USER_POINTS");
    }
}
