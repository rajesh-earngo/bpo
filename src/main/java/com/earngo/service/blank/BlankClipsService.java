package com.earngo.service.blank;

import com.earngo.service.BaseRestService;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by admin on 18/11/16.
 */
@Service
public class BlankClipsService extends BaseRestService {


    public String getBlankClips(MultivaluedMap<String, Object> headerMap, String requestJson) {
        return restCall(headerMap, requestJson, "GET_BLANK_CLIPS");
    }


    public String saveClips(MultivaluedMap<String, Object> headerMap, String requestJson) {
        return restCall(headerMap, requestJson, "SAVE_BLANK_CLIPS");
    }
}


