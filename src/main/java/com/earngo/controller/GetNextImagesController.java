package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.exception.ConnectionFailure;
import com.earngo.exception.UnknownFailureError;
import com.earngo.service.nextimage.GetNextImageService;
import com.earngo.util.EarngoConstants;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/service/nextImage")
public class GetNextImagesController extends BaseController {

    @Autowired
    private GetNextImageService getNextImageService;

    @RequestMapping(value = "/afterSubmit", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNextImagesAfterSubmit(@RequestBody String jsonStr) {
        JSONObject result = getNextImageService.getImagesFromEarngo(getEarngoHeader(), jsonStr, EarngoConstants.NO_OF_CLIPS_AFTER_SUBMIT, false);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/pageLoad", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNextImagesOnPageLoad(@RequestBody String jsonStr) {
        JSONObject result = getNextImageService.getImagesFromEarngo(getEarngoHeader(), jsonStr, EarngoConstants.NO_OF_CLIPS_ON_PAGE_LOAD, true);
        return new ResponseEntity<>(result.toString(), HttpStatus.OK);
    }

    /**
     * Get images for Next Large Image clips
     *
     * @return
     */
    @RequestMapping(value = "/large/pageLoad", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNextLargeImagesOnPageLoad(@RequestBody String jsonStr) {
        JSONObject result = getNextImageService.getLargeImagesFromEarngo(getEarngoHeader(), jsonStr, EarngoConstants.NO_OF_LARGE_CLIPS_ON_PAGE_LOAD, true);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }

    /**
     * @return
     */
    @RequestMapping(value = "/large/afterSubmit", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNextLargeImagesAfterSubmit(@RequestBody String jsonStr) {
        JSONObject result = getNextImageService.getLargeImagesFromEarngo(getEarngoHeader(),jsonStr, EarngoConstants.NO_OF_LARGE_CLIPS_AFTER_SUBMIT, false);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> posImageResult(@RequestBody String jsonStr) {
        JSONObject result = getNextImageService.postResultArrayToEarngo(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }
}
