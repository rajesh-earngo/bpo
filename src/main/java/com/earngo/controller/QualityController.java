package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.service.quality.QualityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Rajesh on 10/7/16.
 */
@RestController
@RequestMapping(value = "/service/quality")
public class QualityController extends BaseController {

    @Autowired
    private QualityService qualityService;

    @RequestMapping(value = "/forms", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> fetchAllForms(@RequestBody String jsonStr) {
        String result = qualityService.fetchAllForms(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    /**
     * Fetch List of Images
     * @param jsonStr
     * @return
     */
    @RequestMapping(value = "/images/all", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> fetchFormImages(@RequestBody String jsonStr) {
        String result = qualityService.fetchFormImages(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    /**
     * Fetch Single Image data
     * @param jsonStr
     * @return
     */
    @RequestMapping(value = "/imageData", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> fetchImagesData(@RequestBody String jsonStr) {
        String result = qualityService.fetchImagesData(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }
}