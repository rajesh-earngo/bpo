package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.service.profile.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by admin on 30/7/16.
 */
@RestController
@RequestMapping(value = "/service/profile")
public class UserProfileController extends BaseController {

    @Autowired
    private UserProfileService userProfileService;

    @RequestMapping(value = "/fetch", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public String fetchProfile(@RequestBody String jsonStr) {
        return userProfileService.getProfile(getEarngoHeader(), jsonStr);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public String updateProfile(@RequestBody String jsonStr) {
        return userProfileService.updateProfile(getEarngoHeader(), jsonStr);
    }

    @RequestMapping(value = "/points/redeem", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public String redeemPoints(@RequestBody String jsonStr) {
        return userProfileService.redeemPoints(getEarngoHeader(), jsonStr);
    }

    @RequestMapping(value = "/points/fetch", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public String fetchPoints(@RequestBody String jsonStr) {
        return userProfileService.fetchPoints(getEarngoHeader(), jsonStr);
    }
}
