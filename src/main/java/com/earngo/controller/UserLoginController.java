package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.model.LoginUser;
import com.earngo.service.login.UserLoginService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/service/login")
public class UserLoginController extends BaseController {

    @Autowired
    private UserLoginService loginService;

    @RequestMapping(value = "/validate", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> validateUserLogin(@RequestBody String jsonStr) {
        String result = loginService.validateUserLogin(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/task/active", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getActiveTasksForBPO(@RequestBody String jsonStr) {
        String result = loginService.getActiveTaskForBPO(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);

    }

    @RequestMapping(value = "/device/init", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> initializeDevice(@RequestBody String jsonStr) {
        httpRequest.getSession().setAttribute("RAJA", "RAJESH");
        String result = loginService.initializeDevice(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/service/test", method = {RequestMethod.GET, RequestMethod.POST}, headers = HEADER_APP_JSON)
    public ResponseEntity<String> test(@RequestBody String jsonStr) {
        return new ResponseEntity<String>("Test Method Called", HttpStatus.OK);
    }

}
