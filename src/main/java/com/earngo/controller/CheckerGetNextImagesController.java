package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.service.nextimage.CheckerGetNextImageService; 
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController

@RequestMapping(value = "/service/checker/nextImage")
public class CheckerGetNextImagesController extends BaseController {


    @Autowired
    private CheckerGetNextImageService checkerGetNextImageService;

    @RequestMapping(value = "/save", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> posImageResult(@RequestBody String jsonStr) {
        JSONObject result = checkerGetNextImageService.postResultToEarngo(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }


    /**
     * Get images for Next Large Image clips
     *
     * @return
     */
    @RequestMapping(value = "/pageLoad", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNexCheckerImagesOnPageLoad() {
        JSONObject result = checkerGetNextImageService.getCheckerImagesFromEarngo(getEarngoHeader(), 2, true);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }

    /**
     * @return
     */
    @RequestMapping(value = "/afterSubmit", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNextCheckerImagesAfterSubmit() {
        JSONObject result = checkerGetNextImageService.getCheckerImagesFromEarngo(getEarngoHeader(), 1, false);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }


    /**
     * Get images for Next Large Image clips
     *
     * @return
     */
    @RequestMapping(value = "/large/pageLoad", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNexCheckerLargeImagesOnPageLoad(@RequestBody String jsonStr) {
        JSONObject result = checkerGetNextImageService.getCheckerLargeImagesFromEarngo(getEarngoHeader(), jsonStr, 2, true);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }


    /**
     * Get images for Next Large Image clips after submit
     *
     * @return
     */
    @RequestMapping(value = "large/afterSubmit", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNextCheckerLargeImagesAfterSubmit(@RequestBody String jsonStr) {
        JSONObject result = checkerGetNextImageService.getCheckerLargeImagesFromEarngo(getEarngoHeader(),jsonStr, 1, false);
        return new ResponseEntity<String>(result.toString(), HttpStatus.OK);
    }



}
