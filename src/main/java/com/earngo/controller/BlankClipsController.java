package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.service.blank.BlankClipsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by admin on 18/11/16.
 */
@RestController
@RequestMapping(value = "/service/blank")
public class BlankClipsController extends BaseController {

    @RequestMapping(value = "/clips/save", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> saveClips(@RequestBody String jsonStr) {
        return new ResponseEntity<>(blankClipsService.saveClips(getEarngoHeader(), jsonStr),HttpStatus.OK);
    }

    @RequestMapping(value = "/clips/fetch", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getBlankClips(@RequestBody String jsonStr) {
        return new ResponseEntity<>(blankClipsService.getBlankClips(getEarngoHeader(), jsonStr),HttpStatus.OK);
    }

    @Autowired
    private BlankClipsService blankClipsService;
}