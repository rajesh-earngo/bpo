package com.earngo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Rajesh on 15/5/16.
 */

@PropertySource(value = { "classpath:config/application.properties" })
@RestController
public class TestController {

    @Autowired
    private Environment environment;

    @RequestMapping(value = { "/test" }, method = RequestMethod.GET)
    public String getTestMessage(){
        return "Hello, This is Customer Application : "+environment.getRequiredProperty("app.server.url");
    }
}

