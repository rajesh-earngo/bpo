package com.earngo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * Created by Rajesh on 16/5/16.
 */

@Configuration
@PropertySource(value = { "classpath:config/application.properties" })
public class AppProperties {

    @Autowired
    private Environment environment;

    public String getProperty(String property) {
        return environment.getRequiredProperty(property);
    }

    public String getAppUrl() {
        return getProperty("app.server.url");
    }

    public String getTemplateUploadPath() {
        return getProperty("templates.upload.location");
    }
}
