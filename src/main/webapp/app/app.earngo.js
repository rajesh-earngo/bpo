/**
 * Created by Rajesh on 16/4/16.
 */

var dependencyModules = ['ui.router',
    'app.earngo.mock.backend',
    'app.modules.login',
    'app.modules.signin', // Home signin
    'app.modules.config',
    'app.modules.rest.service',
    'app.modules.templates',
    'app.modules.dashboard',
    'app.modules.exception',
    'app.modules.forms',
    'app.modules.quality',
    'app.services.alert',
    'app.modules.blankClips', // Blank clips
    'app.services.redeem', // Home app redeem service
    'app.services.userpoints', // Home app userpoints service
    'app.services.matchform', // Failed Form - Match service
    'app.services.formEntry', // Failed Form - Entry service
    'app.modules.home', // BPO Home
    'app.modules.timer', // Timer
    'app.modules.profile', // Home app profile
    'app.modules.redeem', // Home app redeem
    'app.modules.language', // languages and translations
    'app.modules.components.navigation.top', // top menu
    'app.modules.components.navigation.side', // sidebar navigation
    'app.modules.components.modal.alert', // alert modal
    'app.modules.components.modal.help', // help modal
    'app.modules.components.modal.timeout', // timeout modal
    'app.modules.dataEntry.urgent', // BPO urgent clips
    'app.modules.dataEntry.large', // BPO large clips
    'app.modules.dataEntry.checker.urgent', // Checker urgent
    'app.modules.dataEntry.checker.large', // Checker large
    'app.modules.matchForm', // Failed Form - Match
    'app.modules.formEntry', // Failed Form - Entry
    'app.modules.components.card', // cards
    'pascalprecht.translate', // translation
    'ngValidate', // form validation
    'ngCookies', // cookies
    'dcbImgFallback', // image placeholder and fallback: https://github.com/dcohenb/angular-img-fallback
    'disableAll' // disable any element (e.g. div) contents and clicks
    ];

var defaultViews = {
    'modal': {
        templateUrl: 'app/modules/components/modal/helpAndAlert.html'
    },
    'menu': {
        templateUrl: "app/modules/components/navigation/top/navigation.top.view.html"
    },
    'sidebar': {
        templateUrl: 'app/modules/components/navigation/side/navigation.side.view.html'
    },
    'translation': {
        templateUrl: 'app/modules/languages/languages.view.html'
    }
};

function AppRoutes($stateProvider, $urlRouterProvider, STATES_URLS) {
    $stateProvider
        .state('login', {
            url: STATES_URLS.login.url,
            // templateUrl: 'app/modules/login/login.view.html'
            // Replacing with Home app signin
            'views': {
                '' : {
                    templateUrl: 'app/modules/signin/signin.html'
                },
                'modal': {
                    templateUrl: 'app/modules/components/modal/helpAndAlert.html'
                },
                'translation': {
                    templateUrl: 'app/modules/languages/languages.view.html'
                }
            }
        })
        .state('main', {
            url: '/main',
            // templateUrl: 'app/modules/main.view.html'
            views: $.extend({}, defaultViews, {
                "": {
                    templateUrl: 'app/modules/main.view.html'
                },
                'translation': {} // remove translation after login
            })
        })
        .state('main.forms', {
            url: STATES_URLS['main.forms'].url,
            templateUrl: 'app/modules/forms/forms.view.html'
        })
        .state('main.templates', {
            url: STATES_URLS['main.templates'].url,
            templateUrl: 'app/modules/templates/templates.view.html'
        })
        .state('main.exception', {
            url: STATES_URLS['main.exception'].url,
            templateUrl: 'app/modules/exception/exception.view.html'
        })
        .state('main.quality', {
            url: STATES_URLS['main.quality'].url,
            templateUrl: 'app/modules/quality/quality.view.html'
        })
        // using Home profile instead of customer
        // keeping this here untouched
        .state('main.profile', {
            url: STATES_URLS['main.profile'].url,
            templateUrl: 'app/modules/profile/profile.view.html'
        })
        .state('blankClips', {
            url: STATES_URLS['blankClips'].url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/blank/blank.view.html'
                }
            })
        })
        .state('profile', {
            url: STATES_URLS.profile.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/profile/profile.html'
                }
            })
        })
        .state('redeem', {
            url: STATES_URLS.redeem.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/redeem/redeem.html'
                }
            })
        })
        .state('main.dashboard', {
           url: STATES_URLS['main.dashboard'].url,
           templateUrl: 'app/modules/dashboard/db.view.html'
        })
        .state('logout', {
            url: '/logout',
            // templateUrl:  'app/modules/login/login.view.html'
            'controller': function ($state, $cookies) {
                // remove top and side menu and goto login state
                $cookies.remove('topMenu');
                $cookies.remove('sideMenu');
                $state.go('login');
            }
        })
        .state('home', {
            url: STATES_URLS.home.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/home/home.html'
                }
            })
        })
        .state('urgent', {
            url: STATES_URLS.urgent.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/dataentry/urgent/urgent.view.html',
                },
                'modal': {
                    templateUrl: 'app/modules/components/modal/modal.view.html'
                },
                'timer': {
                    templateUrl: 'app/modules/timer/timer.view.html'
                }
            })
        })
        .state('large', {
            url: STATES_URLS.large.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/dataentry/large/large.view.html'
                },
                'modal': {
                    templateUrl: 'app/modules/components/modal/modal.view.html'
                },
                'timer': {
                    templateUrl: 'app/modules/timer/timer.view.html'
                }
            })
        })
        .state('matchForm', {
            url: STATES_URLS.matchForm.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/failedform/match/match-form.html'
                },
                'modal': {
                    templateUrl: 'app/modules/components/modal/alertOnly.view.html'
                }
            })
        })
        .state('formEntry', {
            url: STATES_URLS.formEntry.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/failedform/entry/form-entry.html'
                },
                'modal': {
                    templateUrl: 'app/modules/components/modal/alertOnly.view.html'
                },
            })
        })
        .state('checkerUrgent', {
            url: STATES_URLS.checkerUrgent.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/dataentry/checker/urgent/checker-entry.html'
                }
            })
        })
        .state('checkerLarge', {
            url: STATES_URLS.checkerLarge.url,
            views: $.extend({}, defaultViews, {
                '': {
                    templateUrl: 'app/modules/dataentry/checker/large/checker-large.html'
                }
            })
        });
    $urlRouterProvider.otherwise('/login');
}

angular.module("earngo.customer", dependencyModules)
    .config(AppRoutes)
    .run(function ($rootScope) {
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            console.log("State Changed", toState);

        });
    });