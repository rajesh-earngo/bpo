/**
 * Created by Rajesh on 16/4/16.
 */
(function () {
  'use strict';

  function AlertService() {

    //ref - https://github.com/CreativeDream/jquery.modal
    this.alert = function (msg, title, callback) {
      modal({
        type: 'alert',
        title: title ? title : 'Alert',
        text: msg ? msg : "Alert Message!",
        center: false,
        callback: callback
      });
    };

    this.confim = function () {
      modal({
        type: 'confirm',
        title: 'Confirm',
        text: 'Are you sure you want to delete your brain?',
        callback: function (result) {
          alert(result);
        }
      });
    };
    this.warning = function (msg, title, callback) {
      modal({
        type: 'warning',
        title: title ? title : 'Warning',
        text: msg ? msg : ' Warning message!',
        center: false,
        callback: callback
      });
    };
    this.error = function (msg, title, callback) {
      modal({
        type: 'error',
        title: title ? title : 'Error',
        text: msg ? msg : ' Error message!',
        center: false,
        callback: callback
      });
    };
    return this;
  }


  AlertService.$inject = [];

  angular.module('app.services.alert', [])
    .service('alertService', AlertService);

})();