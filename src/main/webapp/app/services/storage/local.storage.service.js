/**
 * Created by Rajesh on 16/4/16.
 */
function EarngoStorage() {

    var activeLanguage = "en_gb";
    var customerId = "CUST_SRNO";
    var earngoIdItem = "EARNGO_ID";
    var deviceIdItem = "DEVICE_ID";
    var userIdItem = "USER_ID";
    var userName = "USER_NAME";
    var sessionTokenVarItem = "SESSION_TOKEN_ID";
    var queueNo= "QUEUE_NO";
    var largeQueueNo= "LARGE_QUEUE_NO";
    var checkerQueueNo= "CHECKER_QUEUE_NO";

    var rememberMeIdItem = "REMEMBER_ME_ID";
    var rememberMePwdItem = "REMEMBER_ME_PASSWORD";

    this.setLanguage = function (langCode) {
        localStorage.setItem(activeLanguage, langCode);
    };

    this.getLanguage = function () {
        return localStorage.getItem(activeLanguage);
    };

    this.setSessionToken = function (st) {
        localStorage.setItem(sessionTokenVarItem, st);
    };

    this.getSessionToken = function () {
        return localStorage.getItem(sessionTokenVarItem);
    };

    this.clearSessionToken = function () {
        localStorage.removeItem(sessionTokenVarItem);
    };

    // device Id
    this.setDeviceId = function (deviceId) {
        localStorage.setItem(deviceIdItem, deviceId);
    };
    this.getDeviceId = function () {
        return localStorage.getItem(deviceIdItem);
    };
    this.clearDeviceId = function () {
        localStorage.removeItem(deviceIdItem);
    };

    this.setEarngoId = function (id) {
        localStorage.setItem(earngoIdItem, id);
    };

    this.getEarngoId = function () {
        return localStorage.getItem(earngoIdItem);
    };

    this.clearEarngoId = function () {
        localStorage.removeItem(earngoIdItem);
    };

    this.setUserId = function (id) {
        localStorage.setItem(userIdItem, id);
    };

    this.getUserId = function () {
        return localStorage.getItem(userIdItem);
    };

    this.clearUserId = function () {
        localStorage.removeItem(userIdItem);
    };

    /*
     this.printAllLocalStoraeItems(){

     };
     */
    this.flushClearStorageItems = function () {
        localStorage.removeItem(earngoIdItem);
        localStorage.removeItem(deviceIdItem);
        localStorage.removeItem(sessionTokenVarItem);
        localStorage.removeItem(userIdItem);
        localStorage.removeItem(userName);
    };


    //Remember me username
    this.setRememberMeId = function (id) {
        localStorage.setItem(rememberMeIdItem, id);
    };

    this.getRememberMeId = function () {
        return localStorage.getItem(rememberMeIdItem);
    };

    //Remember me password
    this.setRememberMePassword = function (pwd) {
        localStorage.setItem(rememberMePwdItem, pwd);
    };

    this.getRememberMePassword = function () {
        return localStorage.getItem(rememberMePwdItem);
    };

    this.clearRememberMe = function () {
        localStorage.removeItem(rememberMeIdItem);
        localStorage.removeItem(rememberMePwdItem);
    };


    this.setUserName = function (name) {
        localStorage.setItem(userName, name);
    };

    this.getUserName = function () {
        return localStorage.getItem(userName);
    };

    this.clearUserName = function () {
        localStorage.removeItem(userName);
    };

    this.setCustomerId = function (customerId) {
        localStorage.setItem(customerId, customerId);
    };

    this.getCustomerId = function () {
        return localStorage.getItem(customerId);
    };

    this.clearCustomerId = function () {
        localStorage.removeItem(customerId);
    };

    this.setQueueName = function (qName) {
        localStorage.setItem(queueNo, qName);
    };

    this.getQueueName = function () {
        return localStorage.getItem(queueNo);
    };

    this.setLargeQueueName = function (qName) {
        localStorage.setItem(largeQueueNo, qName);
    };

    this.getLargeQueueName = function () {
        return localStorage.getItem(largeQueueNo);
    };

    this.setCheckerQueueName = function (qName) {
        localStorage.setItem(checkerQueueNo, qName);
    };

    this.getCheckerQueueName = function () {
        return localStorage.getItem(checkerQueueNo);
    };

};

var earngoStore = new EarngoStorage();
