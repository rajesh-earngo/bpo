(function (angular) {

    var userPointsService = angular.module('app.services.userpoints', []);

    userPointsService.service('userPointsService', function (restService) {

        function getUserPoints() {
            var userPointPromise = restService.httpPostPromise('service/fetchPoints', {});
            return userPointPromise.then(function (payload) {
                console.log("userpoints : ",payload);
                    if (payload && payload.RES_STATUS === 'S') {
                        return payload.WRK_POINTS;
                    } else {
                        return 0;
                    }
                }
            );
        }

        return {
            getUserPoints: getUserPoints,
        }
    });

})(angular);