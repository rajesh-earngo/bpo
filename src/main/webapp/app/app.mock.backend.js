(function (angular) {

    var mockBackend = angular.module('app.earngo.mock.backend', ['ngMockE2E']);

    //////////////////////////////////////////////////
    // Simulate delay in fetching data from backend //
    //////////////////////////////////////////////////
    mockBackend.config(function ($provide) {
        $provide.decorator('$httpBackend', function ($delegate) {
            var proxy = function (method, url, data, callback, headers) {
                var delay = 0;
                // simulate delay only when it's getting new images
                if (url == 'service/nextImage/afterSubmit') {
                    console.log("********************************");
                    console.log("delay");
                    console.log("********************************");
                    // delay = 10000;
                }
                var interceptor = function () {
                    var _this = this,
                        _arguments = arguments;
                    setTimeout(function () {
                        callback.apply(_this, _arguments);
                    }, delay);
                };
                return $delegate.call(this, method, url, data, interceptor, headers);
            };
            for (var key in $delegate) {
                proxy[key] = $delegate[key];
            }
            return proxy;
        });
    });

    mockBackend.run(['$httpBackend', 'clipTypeEnum', 'clipPaths', function ($httpBackend, clipTypeEnum, clipPaths) {

        // Init device
        $httpBackend.whenPOST('service/login/device/init').respond(function (method, url, data) {
            var response = {
                EARNGO_DEVICE_ID: "EARNGO_DEVICE_ID",
                EARNGO_SESSION_TOKEN: "EARNGO_SESSION_TOKEN"
            };
            return [200, response];
        });

        // Sign in
        $httpBackend.whenPOST('service/signin/validateCustomer').respond(function (method, url, data) {
            return [200, {
                RES_STATUS: 'S',
                EARNGO_ID: "EARNGO_ID",
            }];
        });

        //validate Template Name
        $httpBackend.whenPOST('service/forms/validateTemplate').respond(function (method, url, data) {
            //read from match-form-response.js file
            return [200, {
                RES_STATUS: "S", RES_CODE: "101",
                RES_MESSAGE: "NO SIMILAR FORM NAME AVAILABLE FOR THE CUSTOMER"
            }];
        });


        //Register Template Name
        $httpBackend.whenPOST('service/forms/registerTemplate').respond(function (method, url, data) {
            //read from match-form-response.js file
            return [200, {RES_STATUS: "S", RES_CODE: "101"}];
        });

        //Register Template Name
        $httpBackend.whenPOST('service/template/fetch').respond(function (method, url, data) {
            //read from match-form-response.js file
            return [200, selectFormJson];
        });

        //fetch form details
        $httpBackend.whenPOST('service/forms/details').respond(function (method, url, data) {
            //read from match-form-response.js file
            return [200, selectFormJson];
        });


        //fetch Template ROI
        $httpBackend.whenPOST('service/template/fetch/roi').respond(function (method, url, data) {
            //read from match-form-response.js file
            console.log('request data', data);
            if (data["TEMPLATE_ID"] == 'Page1') {
                return [200, zoneMarkedFormRes1];
            }
            return [200, zoneMarkedFormRes1];
            // return [200, zoneMarkedFormRes1];
        });


        /////////////
        // Quality //
        /////////////

        //fetch quality page form details
        $httpBackend.whenPOST('service/quality/forms').respond(function (method, url, data) {
            //read from match-form-response.js file
            return [200, fetchFormDetail];
        });

        //fetch quality page images details
        $httpBackend.whenPOST('service/quality/images/all').respond(function (method, url, data) {
            //read from match-form-response.js file
            return [200, fetchImagesDetail];
        });

        //fetch quality page images details
        $httpBackend.whenPOST('service/quality/imageData').respond(function (method, url, data) {
            //read from match-form-response.js file
            return [200, qualityImgResponse];
        });

        //Post quality page images data details
        $httpBackend.whenPOST('service/quality/imagesDataPost').respond(function (method, url, data) {
            //read from match-form-response.js file
            return [200, qualityDataRes];
        });

        /////////////
        // Sign in //
        /////////////
        $httpBackend.whenPOST('service/login/validate').respond(function (method, url, data) {
            return [200, {
                "RES_STATUS": "S",
                "RESULT": {
                    "RES_CODE": "S001",
                    "RES_MESSAGE": "Success"
                },
                "EARNGO_SESSION_KEY": "testkey",
                "EARNGO_ID": "3",
                "WRK_LANGUAGE_1": "1",
                "WRK_TYPE": "CT",
                "TOP_MENU": [
                    {
                        "SEQ_ORDER": 2,
                        "STATE": "main.forms",
                        "DEFAULT": "N"
                    },
                    {
                        "SEQ_ORDER": 1,
                        "STATE": "main.dashboard",
                        "DEFAULT": "Y"
                    },
                    {
                        "SEQ_ORDER": 3,
                        "STATE": "main.templates",
                        "DEFAULT": "N"
                    },
                    {
                        "SEQ_ORDER": 4,
                        "STATE": "main.exception",
                        "DEFAULT": "N"
                    },
                    {
                        "SEQ_ORDER": 5,
                        "STATE": "main.quality",
                        "DEFAULT": "N"
                    }
                ],
                "SIDE_MENU": [
                    {
                        SEQ_ORDER: 2,
                        STATE: "large",
                        DEFAULT: "N",
                        HREF: "#largeEntry",
                        TITLE: "NAVIGATION.MENU_ITEM.LARGE_CLIPS",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.LARGE_CLIPS",
                        URL: "/largeEntry"
                    },
                    {
                        "SEQ_ORDER": 1,
                        "STATE": "urgent",
                        "DEFAULT": "N",
                        HREF: "#clipUrgentEntry",
                        TITLE: "NAVIGATION.MENU_ITEM.URGENT_CLIPS",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.URGENT_CLIPS",
                        URL: "/clipUrgentEntry"
                    },
                    {
                        "SEQ_ORDER": 3,
                        "STATE": "checkerUrgent",
                        "DEFAULT": "N",
                        HREF: "#checkerEntry",
                        TITLE: "NAVIGATION.MENU_ITEM.CHECKER_URGENT",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.CHECKER_URGENT",
                        URL: "/checkerEntry"
                    },
                    {
                        "SEQ_ORDER": 4,
                        "STATE": "checkerLarge",
                        "DEFAULT": "N",
                        HREF: "#checkerLargeEntry",
                        TITLE: "NAVIGATION.MENU_ITEM.CHECKER_LARGE",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.CHECKER_LARGE",
                        URL: "/checkerLargeEntry"

                    },
                    {
                        "SEQ_ORDER": 5,
                        "STATE": "home",
                        "DEFAULT": "N",
                        HREF: "#dashboard",
                        TITLE: "NAVIGATION.MENU_ITEM.DASHBOARD",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.DASHBOARD",
                        URL: "/dashboard"
                    },
                    {
                        "SEQ_ORDER": 6,
                        "STATE": "matchForm",
                        "DEFAULT": "N",
                        HREF: "#matchform",
                        TITLE: "NAVIGATION.MENU_ITEM.LINK_FORMS",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.LINK_FORMS",
                        URL: "/matchform"
                    },
                    {
                        "SEQ_ORDER": 7,
                        "STATE": "formEntry",
                        "DEFAULT": "N",
                        HREF: "#formentry",
                        TITLE: "NAVIGATION.MENU_ITEM.ENTRY_FORMS",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.ENTRY_FORMS",
                        URL: "/formentry"

                    },
                    {
                        "SEQ_ORDER": 8,
                        "STATE": "profile",
                        "DEFAULT": "N",
                        HREF: "#profile",
                        TITLE: "NAVIGATION.MENU_ITEM.PROFILE",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.PROFILE",
                        URL: "/profile"
                    },
                    {
                        "SEQ_ORDER": 9,
                        "STATE": "redeem",
                        "DEFAULT": "N",
                        HREF: "#redeem",
                        TITLE: "NAVIGATION.MENU_ITEM.REDEEM",
                        TOOLTIP: "NAVIGATION.TOOLTIPS.REDEEM",
                        URL: "/redeem"
                    },
                    {
                        SEQ_ORDER: 10,
                        STATE: "blankClips",
                        DEFAULT: "N",
                        HREF: "#blankClips",
                        TITLE: "NAVIGATION.MENU_ITEM.BLANK_CLIPS",
                        TOOLTIP: "NAVIGATION.MENU_ITEM.BLANK_CLIPS",
                        URL: "/blankClips"
                    }]
            }];
        });

        //////////////////////
        // BPO Urgent Clips //
        //////////////////////

        function generateRandomImageSet(numOfItems) {
            var randomImageSet = [];
            // generate random image sets for testing
            for (var i = 0; i < numOfItems; i++) {
                // use below to use a apecific clip type, refer to clipTypeEnum
                // var keyIndex = 11;
                // use below to randomly choose a clip type
                var clipTypeEnumKeys = Object.keys(clipTypeEnum);
                var keyIndex = Math.floor(Math.random() * clipTypeEnumKeys.length);
                //change below for testing purpose
                //var clipTypeEnumKey = clipTypeEnum.DATE_AND_TIME;
                var clipTypeEnumKey = clipTypeEnum[clipTypeEnumKeys[keyIndex]];
                var clipTypeEnumPath = clipPaths[clipTypeEnumKeys[keyIndex]];

                var randomImage = {
                    // setting large number cause if this is repeated, then id of inputs
                    // would be affected, resulting in errors. e.g. inputs disabled when
                    // it's not supposed to
                    clipNo: Math.floor(Math.random() * 100000000000000),
                    clipType: clipTypeEnumKey,
                    clipPath: clipTypeEnumPath,
                    customMsg: "Custom message here",
                };

                randomImageSet.push(randomImage);
            }

            return randomImageSet;
        }

        function getAllPossibleImageSets() {
            var imageSet = [];
            var clipTypeEnumKeys = Object.keys(clipTypeEnum);
            for (var i = 0; i < clipTypeEnumKeys.length; i++) {
                var clipTypeEnumKey = clipTypeEnum[clipTypeEnumKeys[i]];
                var clipTypeEnumPath = clipPaths[clipTypeEnumKeys[i]];
                // console.log(clipTypeEnumKey);
                imageSet.push({
                    clipNo: Math.floor(Math.random() * 100000000000000),
                    clipType: clipTypeEnumKey,
                    clipPath: clipTypeEnumPath,
                    customMsg: "Custom message here",
                });
            }
            return imageSet;
        }

        /////////////////
        // User Points //
        /////////////////
        $httpBackend.whenPOST('service/profile/points/fetch').respond(function (method, url, data) {
            return [200, {
                userPoints: 999
            }];
        });

        ///////////////////////////
        // Navigation in Sidebar //
        ///////////////////////////
        $httpBackend.whenPOST('service/login/task/active').respond(function (method, url, data) {
            return [200, {
                TASK_COUNT_BPO: 100,
                TASK_COUNT_LARGE: 100,
                TASK_COUNT_EXCEPTION_MATCH: 100,
                TASK_COUNT_CHECKER: 100,
                TASK_COUNT_EXCEPTION_CHECKER: 100,
                TASK_COUNT_EXCEPTION_DE: 100,
                date: new Date().getTime()
            }];
        });

        ///////////////////////
        // Urgent Data Entry //
        ///////////////////////
        $httpBackend.whenPOST('service/nextImage/pageLoad').respond(function (method, url, data) {
            // reference: webapp/dataentry/common/data-fields-type.js

            var defaultClipNumber = 3;
            return [200, {
                // get all possible clips for UI dev purposes, else have to keep refreshing screen
                // comment line below out if you'd like to test with default 3
                // imagesSetOne: getAllPossibleImageSets(),
                imagesSetOne: generateRandomImageSet(defaultClipNumber),
                imagesSetTwo: generateRandomImageSet(defaultClipNumber),
            }];
        });

        $httpBackend.whenPOST('service/nextImage/save').respond(function (method, url, data) {
            return [200, "Status message here"];
        });

        $httpBackend.whenPOST('service/nextImage/afterSubmit').respond(function (method, url, data) {
            var defaultClipNumber = 3;
            return [200, {
                imagesSetOne: generateRandomImageSet(defaultClipNumber),
                // imagesSetOne: [],
            }];
        });

        /////////////////////
        // BPO Large Clips //
        /////////////////////
        function getRandomLargeClip() {
            var defaultClipNumber = 1;
            var whichClip = Math.floor(Math.random() * 2);
            var image = {
                // setting large number cause if this is repeated, then id of inputs
                // would be affected, resulting in errors. e.g. inputs disabled when
                // it's not supposed to
                clipNo: Math.floor(Math.random() * 100000000000000),
                // clipType: clipTypeEnum.LARGE_CLIP,
                // clipPath: clipPaths.LARGE_CLIP,
                customMsg: "Custom message here",
            };
            if (whichClip === 0) {
                image.clipType = clipTypeEnum.LARGE_CLIP;
                image.clipPath = clipPaths.LARGE_CLIP;
            }
            else {
                image.clipType = clipTypeEnum.LARGE_MCQ;
                image.clipPath = clipPaths.LARGE_MCQ;
            }
            return image;
        }

        $httpBackend.whenPOST('service/nextImage/large/pageLoad').respond(function (method, url, data) {
            var defaultClipNumber = 1;
            return [200, {
                imagesSetOne: [getRandomLargeClip()],
                imagesSetTwo: [getRandomLargeClip()]
            }];
        });

        $httpBackend.whenPOST('service/nextImage/large/afterSubmit').respond(function (method, url, data) {
            return [200, {
                imagesSetOne: [getRandomLargeClip()]
            }];
        });

        //////////////////////
        // BPO Failed Forms //
        //////////////////////
        $httpBackend.whenPOST('service/nextMatchFormDetails').respond(function (method, url, data) {

            var matchTestingData = {
                "templateArray": [
                    {
                        "template_id": 1,
                        "template_path": "assets/img/forma.jpg",
                        "form_id": 1,
                        "sub_form_id": 1,
                        "form_name": "earngo_testform1",
                        "template_name": "earngo_testform1_1",
                        "cust_name": "1"
                    },

                    {
                        "template_id": 2,
                        "template_path": "assets/img/formb.jpg",
                        "form_id": 1,
                        "sub_form_id": 1,
                        "form_name": "earngo_testform1",
                        "template_name": "earngo_testform1_1",
                        "cust_name": "1"
                    },
                    {
                        "template_id": 3,
                        "template_path": "assets/img/formc.jpg",
                        "form_id": 1,
                        "sub_form_id": 1,
                        "form_name": "earngo_testform1",
                        "template_name": "earngo_testform1_1",
                        "cust_name": "1"
                    },
                    {
                        "template_id": 4,
                        "template_path": "assets/img/formd.jpg",
                        "form_id": 1,
                        "sub_form_id": 1,
                        "form_name": "earngo_testform1",
                        "template_name": "earngo_testform1_1",
                        "cust_name": "1"
                    },
                    {
                        "template_id": 5,
                        "template_path": "assets/img/forme.jpg",
                        "form_id": 1,
                        "sub_form_id": 1,
                        "form_name": "earngo_testform1",
                        "template_name": "earngo_testform1_1",
                        "cust_name": "1"
                    }
                ],
                "cust_srno": 1,
                "unknown_image_path": "assets/img/13012015125857-0005.jpg",
                "unknown_image_id": 2,
                "SEQ_NO": 1,
                "RES_STATUS": "S",
                "RES_CODE": "101",
                "RES_MESSAGE": "SUCCESSFUL"
            };

            return [200, matchTestingData];
        });

        $httpBackend.whenPOST('service/failedImageDetails').respond(function (method, url, data) {
            var failedFormEntryTestData = {
                "RES_STATUS": "S",
                "custFormPath": "assets/img/13012015125857-0005.jpg",
                "custFormId": "1",
                "fieldsArray": [
                    {
                        "clipNo": 1,
                        "imgNo": "1",
                        "clipName": "1_d",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "SNS",
                        "clipValue": "",
                    },
                    {
                        "clipNo": 2,
                        "imgNo": "1",
                        "clipName": "1_a",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "NM",
                        "clipValue": "",
                    },
                    {
                        "clipNo": 3,
                        "imgNo": "1",
                        "clipName": "1_b",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "LT",
                        "clipValue": "",
                    }, {
                        "clipNo": 4,
                        "imgNo": "1",
                        "clipName": "1_c",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "ST",
                        "clipValue": "",
                    }, {
                        "clipNo": 4,
                        "imgNo": "1",
                        "clipName": "1_c",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "ST",
                        "clipValue": "",
                    }, {
                        "clipNo": 4,
                        "imgNo": "1",
                        "clipName": "1_c",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "ST",
                        "clipValue": "",
                    }, {
                        "clipNo": 4,
                        "imgNo": "1",
                        "clipName": "1_c",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "ST",
                        "clipValue": "",
                    }, {
                        "clipNo": 4,
                        "imgNo": "1",
                        "clipName": "1_c",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "ST",
                        "clipValue": "",
                    }, {
                        "clipNo": 4,
                        "imgNo": "1",
                        "clipName": "1_c",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "ST",
                        "clipValue": "",
                    }, {
                        "clipNo": 4,
                        "imgNo": "1",
                        "clipName": "1_c",
                        "customMessage": "Fill the textbox",
                        "imgPath": "assets/img/13012015125857-0005.jpg",
                        "clipType": "ST",
                        "clipValue": "",
                    }
                ]
            };

            return [200, failedFormEntryTestData];
        });

        ///////////////////
        // Checker Clips //
        ///////////////////
        function getMockClipResponse(numOfResponses) {
            var mockClipData = generateRandomImageSet(numOfResponses);
            for (var i = 0; i < mockClipData.length; i++) {
                var numOfUserValues = 1;
                var mockWorkerData = getMockWorkerData(numOfUserValues);
                mockClipData[i].checkerDataArr = getMockWorkerData(numOfUserValues);
            }
            console.log(mockClipData);
            return mockClipData;
        }

        function getMockLargeClipResponse(numOfResponses) {
            var mockClipData = getRandomLargeClip();
            // for (var i = 0; i < mockClipData.length; i++) {
            var numOfUserValues = 9;
            var mockWorkerData = getMockWorkerData(numOfUserValues);
            mockClipData.checkerDataArr = getMockWorkerData(numOfUserValues);
            // }
            return mockClipData;
        }

        function getMockWorkerData(numOfUserValues) {
            var checkerDataArr = [];

            for (var i = 0; i < numOfUserValues; i++) {
                checkerDataArr.push({
                    "DISTRIBUTOR_NUM": Math.floor(Math.random() * 100000000000000), // id of the distributor (data entry worker)
                    "OUTTRAY_INPUT": "*",  // what the distributor (worker) entered, * = unclear
                    "OUTTRAY_ACTION": 2     // Reference: /checker/dataentry/common/data-fields-type.js => map to Outtray Action  - action is 2 when value is *
                });
            }
            ;
            return checkerDataArr;
        }

        ////////////////////
        // Checker Urgent //
        ////////////////////

        $httpBackend.whenPOST('service/getNextCheckerImagesOnPageLoad').respond(function (method, url, data) {

            var defaultNumOfSmallClips = 1;
            return [200, {
                // imagesSetOne: getAllPossibleImageSets(),
                imagesSetOne: getMockClipResponse(defaultNumOfSmallClips),
                imagesSetTwo: getMockClipResponse(defaultNumOfSmallClips),
            }];
        });

        $httpBackend.whenPOST('service/getNextCheckerImagesAfterSubmit').respond(function (method, url, data) {
            var defaultNumOfSmallClips = 1;
            return [200, {
                imagesSetOne: getMockClipResponse(defaultNumOfSmallClips),
            }];
        });

        ///////////////////
        // Checker Large //
        ///////////////////
        $httpBackend.whenPOST('service/getNextCheckerLargeImagesOnPageLoad').respond(function (method, url, data) {
            var defaultNumOfLargeClips = 1;
            return [200, {
                imagesSetOne: [getMockLargeClipResponse(defaultNumOfLargeClips)],
                imagesSetTwo: [getMockLargeClipResponse(defaultNumOfLargeClips)]
            }];
        });

        $httpBackend.whenPOST('service/getNextCheckerLargeImagesAfterSubmit').respond(function (method, url, data) {
            var defaultNumOfLargeClips = 1;
            return [200, {
                imagesSetOne: [getMockLargeClipResponse(defaultNumOfLargeClips)]
            }];
        });


        ///////////////////
        // Blank Clips ///
        ///////////////////
        $httpBackend.whenPOST('service/blank/clips/fetch').respond(function (method, url, data) {
            return [200, blankTestData];
        });

        $httpBackend.whenPOST('service/blank/clips/save').respond(function (method, url, data) {
            return [200, {
                "RES_STATUS": "S",
                "RES_CODE": "101",
                "RES_MESSAGE": "Saved Successfully"
            }];
        });

        ///////////////////
        // Fetch Profile //
        ///////////////////
        $httpBackend.whenPOST('service/fetchProfile').respond(function (method, url, data) {
            return [
                200,
                [
                    {
                        WRK_ADDRESS_1: null,
                        WRK_BRANCH: null,
                        WRK_MIDDLENAME: null,
                        WRK_PAYPALID: null,
                        WRK_ACCOUNT_NUM: null,
                        SQ_ANSWER: null,
                        WRK_PAYMODE: null,
                        WRK_PROFESSION: null,
                        WRK_SKILL: null,
                        WRK_BANK_NAME: null,
                        WRK_LASTNAME: "N",
                        WRK_EMAIL: "",
                        WRK_MOBILE: null,
                        WRK_FIRSTNAME: "Karthick",
                        SQ_CODE: null,
                        WRK_COUNTRY: null,
                        WRK_BANK_CODE: null
                    }
                ]
            ];
        });

        /////////////////
        // User Points //
        /////////////////
        $httpBackend.whenPOST('service/fetchPoints').respond(function (method, url, data) {
            return [200, {
                RES_STATUS: "S",
                WRK_POINTS: 999
            }];
        });

        ///////////////////////////////////////////////////
        // Catch-all pass through for all other requests //
        ///////////////////////////////////////////////////
        $httpBackend.whenPOST(/.*/).passThrough();
        $httpBackend.whenGET(/.*/).passThrough();
        $httpBackend.whenPUT(/.*/).passThrough();
        $httpBackend.whenDELETE(/.*/).passThrough();
    }]);

})(angular);