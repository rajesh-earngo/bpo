(function () {
  'use strict';

  function FormsController(formsService, formsModelFactory, alertService) {
    var ctrl = this;
    ctrl.vm = formsModelFactory.getModel();

    ctrl.upload = function () {
      if (validatePageSize(ctrl.vm.uploadFiles)) {
        if (validatePageNo(ctrl.vm.uploadFiles)) {
          formsService.upload(ctrl.vm);
        } else {
          alertService.warning('Please verify the Page No !');
        }
      } else {
        alertService.warning('Please upload images with size Greater than 250KB !');
      }
    };

    ctrl.validateTemplateName = function () {
      formsService.validateTemplateName(ctrl.vm);
    };

    ctrl.pageNoChanged = function () {
      formsService.pageNoChanged(ctrl.vm);
    };

    ctrl.registerForms = function () {
      formsService.registerForms(ctrl.vm);
    };

    ctrl.init = function(){
      formsService.fetchFormsDetail(ctrl.vm);
    };


    //validate the Page Seq No
    function validatePageNo(files) {
      var len = files.length;
      for (var index = 0; index < len; index++) {
        var pageNo = files[index].pageNo;
        if (pageNo == (index + 1)) {
        } else {
          return false;
        }
      }
      return true;
    }

    //validate the Page Size
    function validatePageSize(files) {
      var len = files.length;
      for (var index = 0; index < len; index++) {
        var pageSize = files[index].size;
        if (pageSize >= 250000) {
        } else {
          return false;
        }
      }
      return true;
    }
  }

  FormsController.$inject = ['formsService', 'formsModelFactory', 'alertService'];

  angular
    .module('app.modules.forms', ['app.modules.forms.service'])
    .controller('formsController', FormsController);

})();