(function () {
  'use strict';

  function FormsModelFactory() {

    function FormsModel() {
      this.isSinglePage = null;
      this.isBarcodePage = 'N';
      this.imagePageNo = null;
      this.uploadFiles = [];
      this.uniqueFormName = null;
      this.isFileUploaded = false;
      this.formCategory = null;
      this.formCategoryList = null;
      this.isShowUpload = false;
      this.error = {};
      ///this.language = 'en';
      this.customerId = earngoStore.getCustomerId();
    }

    var factory = {};
    factory.getModel = function () {
      return new FormsModel();
    };

    return factory;
  }


  function FormsService(Upload, $timeout, restService, config, $q, alertService) {

    var service = {};
    /**
     * Upload files
     * @param model
     */
    service.upload = function (model) {
      var isUploadSuccess = true;
      angular.forEach(model.uploadFiles, function (file) {
        file.upload = Upload.upload({
          url: 'service/forms/upload',
          data: {file: file}
        });

        file.upload.then(function (response) {
          if (response.data.status === 'F') {
            isUploadSuccess = false;
            console.log('Error in File upload', response.data)
          }
          $timeout(function () {
            file.result = response.data;
          });
        }, function (response) {
          console.log('Error in File upload', response.data);
          isUploadSuccess = false;
        }, function (evt) {
          //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        }).then(function () {
          if (isUploadSuccess) {
            model.isFileUploaded = true;
            alertService.alert('Templates uploaded.', 'Status');
          } else {
            alertService.error('Templates Uploading failed.', 'Status');
          }
        });
      });
    };


    /**
     * Validate the Template/Form Name exist in System or Not
     * @param model
     * @returns {*}
     */
    service.validateTemplateName = function (model) {
      var validateForm = {
        CUST_SRNO: model.customerId,
        FORM_NAME: model.uniqueFormName
      };

      var promise = restService.httpPostPromise(config.VALIDATE_TEMPLATE, validateForm)
        .then(function (result) {
          console.log(result);
          model.error.templateName = result.RES_MESSAGE;
          model.error.isTemplateError = true;
          model.isShowUpload = true;
        });
      return promise;
    };


    service.pageNoChanged = function (model) {
      model.uploadFiles.sort(function (file1, file2) {
        return file1.pageNo - file2.pageNo;
      });
    };


    /**
     * Register the template with each template at a time
     * @param model
     * @returns {*}
     */
    service.registerForms = function (model) {
      var selectedCatName = null;
      angular.forEach(model.formCategoryList, function (item) {
        if (item.categoryId == model.formCategory) {
          selectedCatName = item.categoryName;
        }
      });

      var allRegFormsPromise = [];
      //loop
      model.uploadFiles.forEach(function (file) {
        var regForm = {
          CUST_SRNO: model.customerId,
          FORM_NAME: model.uniqueFormName,
          PAGE_NO: file.pageNo,
          IMAGE_NAME: file.name,
          PAGE_LINKAGE: model.isBarcodePage === 'Y' ? 1 : 0,
          MP: 'Y',
          IMAGE_TYPE: file.type,
          CATEGORY_ID: model.formCategory,
          CATEGORY_NAME: selectedCatName
        };
        //push to array
        var promise = restService.httpPostPromise(config.REGISTER_TEMPLATE, regForm);
        allRegFormsPromise.push(promise);
      });

      return $q.all(allRegFormsPromise).then(function (results) {
        var flag = true;
        results.forEach(function (item) {
          if (item.RES_STATUS === 'F') {
            flag = false;
          }
        });
        if (flag === true) {
          alertService.alert('Templates registered.', 'Status');
        }
      });
    };

    /**
     * Fetch the form details
     * @param model
     * @returns {*}
     */
    service.fetchFormsDetail = function (model) {
      var reqFetchForm = {
        CUST_SRNO: model.customerId
      };

      var promise = restService.httpPostPromise(config.FETCH_ALL_TEMPLATE_DETAIL, reqFetchForm)
        .then(function (result) {
          model.formCategoryList = result.CATEGORY_DETAILS;
        });
      return promise;
    };

    return service;
  }

  FormsService.$inject = ['Upload', '$timeout', 'restService', 'config', '$q', 'alertService'];
  FormsModelFactory.$inject = [];

  angular
    .module('app.modules.forms.service', ['ngFileUpload'])
    .service('formsService', FormsService)
    .service('formsModelFactory', FormsModelFactory);

})
();