(function () {
    'use strict';


    function BlankClipsController(blankClipsService, blankClipsModelFactory) {

        this.blankClipsService = blankClipsService;
        this.vm = blankClipsModelFactory.getInstance();

    };

    BlankClipsController.prototype.init = function () {
        this.loadClips();
    };

    BlankClipsController.prototype.refresh = function () {
        this.loadClips();
    };

    BlankClipsController.prototype.loadClips = function () {
        var ctrl = this;
        ctrl.blankClipsService.loadClips(ctrl.vm)
            .then(function () {
                ctrl.vm.updateModelFields();
            });
    };

    BlankClipsController.prototype.saveClips = function () {
        var ctrl = this;
        ctrl.blankClipsService.saveClips(ctrl.vm)
            .then(function () {
                ctrl.vm.updateModelFields();
                ctrl.loadClips();
            });
    };

    BlankClipsController.prototype.allClipsSelected = function () {
        if (this.vm.isAllClipsSelected) {
            this.vm.setAllClipsNotBlank(true);
        } else {
            this.vm.setAllClipsNotBlank(false);
        }
    };


    BlankClipsController.$inject = ['blankClipsService', 'blankClipsModelFactory'];

    angular
        .module('app.modules.blankClips', ['app.modules.blankClips.service'])
        .controller('blankClipsController', BlankClipsController);

})();