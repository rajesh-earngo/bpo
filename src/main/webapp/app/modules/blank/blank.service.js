(function () {
    'use strict';


    function BlankClipsModelFactory() {

        function BlankClipsModel() {
            this.blankClipsList = [];
            this.isClipsNotAvailable = true;
            this.isAllClipsSelected = false;

        }

        BlankClipsModel.prototype.updateModelFields = function () {
            //update fields which required to check after operation
            this.isClipsNotAvailable = (this.blankClipsList.length === 0);
        };

        BlankClipsModel.prototype.setAllClipsNotBlank = function (flag) {
            this.blankClipsList.forEach(function (item) {
                item.BLANK_STATUS = flag;
            });
        };

        var factory = {};
        factory.getInstance = function () {
            return new BlankClipsModel();
        };
        return factory;
    }

    BlankClipsModelFactory.$inject = [];

    function BlankClipsService(restService, config) {
        var service = {};

        // fetch clips from
        service.loadClips = function (model) {
            var promise = restService.httpPostPromise(config.FETCH_BLANK_CLIPS, {NO_OF_CLIPS: 20})
                .then(function (result) {
                    console.log(result);
                    if (result.RES_STATUS === 'S') {
                        model.blankClipsList = [].concat(result["DATA"]);
                    }
                });
            return promise;
        };

        // save blank clips
        service.saveClips = function (model) {
            model.blankClipsList.forEach(function (item) {
                //update the value as per backend requirement
                item.BLANK_STATUS = item.BLANK_STATUS === true ? 'N' : 'Y';
            });
            var promise = restService.httpPostPromise(config.SAVE_BLANK_CLIPS, model.blankClipsList)
                .then(function (result) {
                    console.log(result);
                    if (result.RES_STATUS === 'S') {
                        //after save  clear it
                        model.blankClipsList = model.blankClipsList.slice(0);
                    }
                    return result;
                });
            return promise;
        };

        return service;
    };


    BlankClipsService.$inject = ['restService', 'config', '$q'];

    angular
        .module('app.modules.blankClips.service', [])
        .service('blankClipsService', BlankClipsService)
        .factory('blankClipsModelFactory', BlankClipsModelFactory);

})();