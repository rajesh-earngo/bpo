'use strict';
var profileModule = angular.module('app.modules.profile', ['ui.router', 'app.earngo.mock.backend', 'app.services.redeem']);

/*Remove this ngroute dependency and replace with angular ui router*/
// profileModule.config(['$routeProvider', function ($routeProvider) {
//     $routeProvider.when('/profile', {
//         templateUrl: 'pages/profile/profile.html',
//     });
// }]);

profileModule.controller('profileController', function ($scope, $location, $translate, restService, redeemService) {
    $scope.user = {};
    $scope.user.firstName = '';
    $scope.user.lastName = '';
    $scope.user.mobileNo = '';
    $scope.user.address = '';
    $scope.user.accountNo = '';
    $scope.user.bankName = '';
    $scope.user.ifscCode = '';
    $scope.user.email = '';

    $scope.user.isInformationVerified = false;


    $scope.initialize = function () {
        var reqObj = {};
        restService.httpPostPromise('service/fetchProfile', reqObj)
            .then(function (data) {
                $scope.user.firstName = data[0].WRK_FIRSTNAME;
                $scope.user.lastName = data[0].WRK_LASTNAME;
                $scope.user.mobileNo = data[0].WRK_MOBILE;
                $scope.user.address = data[0].WRK_ADDRESS_1;
                $scope.user.accountNo = data[0].WRK_ACCOUNT_NUM;
                $scope.user.bankName = data[0].WRK_BANK_NAME;
                $scope.user.ifscCode = data[0].WRK_BRANCH;
                $scope.user.country = data[0].WRK_COUNTRY;
                $scope.user.email = data[0].WRK_EMAIL;
            });
    };


    $scope.redeemPoints = function () {

        if ($scope.user.isInformationVerified == true) {

            if (validateUserValue($scope.user) == false) {
                return false;
            }

            updateData().then(function (value) {
                if (value) {
                    redeemService.setProfileUpdateStatus(true)
                    $location.path('/redeem');
                }
            });
        } else {
            // showModalAlert('Please update your profile and tick the confirmation.');
            // Translations
            showModalAlert('PROFILE.MESSAGES.REDEEM_ERROR');
            return 0;
        }
    }


    $scope.updateAction = function () {
        //validate fields
        if (validateUserValue($scope.user) == false) {
            return false;
        }

        updateData().then(function (value) {
            if (value) {
                redeemService.setProfileUpdateStatus(true) ;
            }
        });
    };


    $scope.cancelAction = function () {
        $location.path('/clipUrgentEntry');
    };


    function updateData() {

        var jsonReq = {};
        jsonReq.WRK_FIRSTNAME = $scope.user.firstName || '';
        jsonReq.WRK_LASTNAME = $scope.user.lastName || '';
        jsonReq.WRK_MOBILE = $scope.user.mobileNo || '';
        jsonReq.WRK_ADDRESS_1 = $scope.user.address || '';
        jsonReq.WRK_ACCOUNT_NUM = $scope.user.accountNo || '';
        jsonReq.WRK_BANK_NAME = $scope.user.bankName || '';
        jsonReq.WRK_BRANCH = $scope.user.ifscCode || '';
        jsonReq.WRK_COUNTRY = $scope.user.country || '';
        jsonReq.WRK_EMAIL = $scope.user.email || '';

        return restService.httpPostPromise('service/updateProfile', jsonReq)
            .then(function (data) {
                var status = data.RES_STATUS;
                if (status === 'S') {
                    // showModalAlert('Profile updated.');
                    // Translations
                    showModalAlert('PROFILE.MESSAGES.PROFILE_UPDATED');
                    return true;
                } else {
                    // showModalAlert('Error in update profile');
                    // Translations
                    showModalAlert('PROFILE.MESSAGES.PROFILE_UPDATED_ERROR');
                    return false;
                }
            });

    }

    function validateUserValue(user) {

        if (user.firstName == null || user.firstName == undefined || user.firstName == '') {
            // showModalAlert('First Name is not valid.');
            // Translations
            showModalAlert('PROFILE.MESSAGES.FIRST_NAME_ERROR');
            return false;
        } else if (user.lastName == null || user.lastName == undefined || user.lastName == '') {
            // showModalAlert('Last Name is not valid.');
            // Translations
            showModalAlert('PROFILE.MESSAGES.LAST_NAME_ERROR');
            return false;
        } else if (user.mobileNo == null || user.mobileNo == undefined || user.mobileNo == '') {
            // showModalAlert('Mobile No is not valid.');
            // Translations
            showModalAlert('PROFILE.MESSAGES.MOBILE_ERROR');
            return false;
        } else if (user.accountNo == null || user.accountNo == undefined || user.accountNo == '') {
            // showModalAlert('Account No is not valid.');
            // Translations
            showModalAlert('PROFILE.MESSAGES.ACCOUNT_NUMBER_ERROR');
            return false;
        } else if (user.ifscCode == null || user.ifscCode == undefined || user.ifscCode == '') {
            // showModalAlert('IFSC Code is not valid.');
            // Translations
            showModalAlert('PROFILE.MESSAGES.IFSC_CODE_ERROR');
            return false;
        } else if (user.bankName == null || user.bankName == undefined || user.bankName == '') {
            // showModalAlert('Bank Name is not valid.');
            // Translations
            showModalAlert('PROFILE.MESSAGES.BANK_NAME_ERROR');
            return false;
        } else if (user.email == null || user.email == undefined || user.email == '') {
            // showModalAlert('Email is not valid.');
            // Translations
            showModalAlert('PROFILE.MESSAGES.EMAIL_ERROR');
            return false;
        }
    }

    function showModalAlert(msg) {
        // Translations
        $translate(msg).then(function (translation) {
            $('#modal-alert').find('.modal-content p').text(translation);
            $('#modal-alert').openModal();
            $('#modal-alert-bttn-ok').focus();
        });
    }

});