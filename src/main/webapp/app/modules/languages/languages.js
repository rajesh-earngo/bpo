(function () {

var languages = [
    {
        code: "en_gb",
        title: "English",
        imgSrc: "img/en_gb.png"
    },
    {
        code: "zh_cn",
        title: "Chinese",
        imgSrc: "img/zh_cn.png"
    },
];

angular.module('app.modules.language', []).directive('eglanguages', function () {
    
    var controller = function ($translate, $scope) {
        var vm = this;
        vm.languages = languages;
        vm.selectedLanguage = languages[0];

        vm.getActiveLanguage = function (langCode) {
            return $translate.use();
        };

        vm.getActiveLanguageClass = function (langCode) {
            // add 'language-item-current' class if the language is used
            return ($translate.use() == langCode) ? 'language-item-current' : '';
        };

        vm.changeLanguage = function (selectedLanguage) {
            // console.log(langCode);
            $translate.use(selectedLanguage.code);
        };
    };

    var linker = function (scope, element, attrs) {
        // init materialize select
        $(element).find('select').material_select();
    };

    return {
        restrict: 'E',
        controller: controller,
        controllerAs: 'langCtrl',
        link: linker,
        scope: {},
        templateUrl: 'app/modules/languages/languages.html'
    };
});

angular.module('earngo.customer').config(['$translateProvider', function ($translateProvider) {
    // reference: http://www.ng-newsletter.com/posts/angular-translate.html
    $translateProvider.useStaticFilesLoader({
        prefix: 'app/modules/languages/translations/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en_gb');
    // escape translated value
    // https://angular-translate.github.io/docs/#/guide/19_security
    $translateProvider.useSanitizeValueStrategy('escape');
}]);

})();
