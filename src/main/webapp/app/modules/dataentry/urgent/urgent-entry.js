'use strict';
var urgentEntryModule = angular.module('app.modules.dataEntry.urgent', ['ui.router', 'app.earngo.mock.backend',
    'disableAll', 'app.modules.timer', 'app.modules.dataentry.common.util']);

urgentEntryModule.controller('urgentEntryController',
    function ($scope, $http, $location, $rootScope, config, restService, timerService, dataEntryUtilService) {

        this.clipDataVm = {
            clipsSetOne: null,
            clipsSetTwo: null,
            showDivClipEntryOne: false,
            showDivClipEntryTwo: false,
            disableDivClipEntryOne: false,
            disableDivClipEntryTwo: false,
            divOne: { // div 1 model
                loading: false // whether the clips are loading
                // @todo Should use model and ng-disabled instead of
                // jquery to disable submit button also? See below
                // submit: { // submit button model
                //   disabled: false // whether submit button is disabled
                // }
            },
            divTwo: { // div 2 model
                loading: false
            }
        };

        var clipData = this.clipDataVm;
        var ctrl = this;

        // when signal to get new data is sent, get new set of data and enable submit buttons
        $scope.$on('GET_NEW_DATA_SET', function (evt) {
            ctrl.loadUrgentDataWithController();
            // @todo replace with model and ng-disabled? See above
            $('.fixed-action-btn .submit').prop('disabled', false);
        });

        // Disable all submit buttons
        $scope.$on('DISABLE_SUBMIT', function (evt) {
            // @todo replace with model and ng-disabled? See above
            $('.fixed-action-btn .submit').prop('disabled', true);
        });

        // get all tasks and display initially
        this.loadUrgentDataWithController = function loadUrgentDataWithController() {

            clipData.disableDivClipEntryOne = false;
            clipData.disableDivClipEntryTwo = false;
            clipData.showDivClipEntryOne = true;
            clipData.showDivClipEntryTwo = false;

            //dataEntryUtilService.alert();

            restService.httpPostPromise(config.URGENT_DATA_ON_PAGE_LOAD, {SQS_NO: earngoStore.getQueueName()})
                .then(function (jsonArr) {
                    if (!jsonArr || !jsonArr.imagesSetOne || jsonArr.imagesSetOne.length === 0) {
                        timerService.stopTimer();
                    }
                    // pass this info to scope so that inputs do not need
                    // to set the html names etc separately, more maintainable
                    clipData.clipsSetOne = dataEntryUtilService.convertToJsObject(jsonArr.imagesSetOne);
                    clipData.clipsSetTwo = dataEntryUtilService.convertToJsObject(jsonArr.imagesSetTwo);
                });
        };


        // Below method submit and poupulate data for DIV One
        this.dataSubmit_One = function dataSubmit_One() {
            var gNxtObjArr = clipData.clipsSetOne;

            // extract data and post into Earngo
            var isAllDataEntered = dataEntryUtilService.retrieveDataFromInputHtmlType(gNxtObjArr);
            if (isAllDataEntered === false) {
                return;
            }

            // data is valid, reset timer by broadcasting RESET_TIMER event
            timerService.startTimer();

            //$("#divClipEntryOne *").prop('disabled', true);
            clipData.disableDivClipEntryOne = true;
            clipData.divOne.loading = true;

            /*saveAndFetchData(gNxtObjArr).then(function (clipsSetOne) {
                clipData.clipsSetOne = clipsSetOne;
                clipData.divOne.loading = false;
            }); */

            //save the clips data
            saveClipsData(gNxtObjArr);
            //clear the clips data
            clipData.clipsSetOne = [];
            //fetch the new clips data
            fetchClipsData(gNxtObjArr).then(function(result){
                clipData.clipsSetOne = result;
                clipData.divOne.loading = false;
            });

            clipData.disableDivClipEntryTwo = false;
            clipData.showDivClipEntryOne = false;
            clipData.showDivClipEntryTwo = true;
            $('#divClipEntryTwo input').not('.unclear').first().focus();
        };


        // Below method submit and poupulate data for DIV Two
        this.dataSubmit_Two = function dataSubmit_Two() {

            var gNxtObjArr = clipData.clipsSetTwo;
            // extract data and post into Earngo
            var isAllDataEntered = dataEntryUtilService.retrieveDataFromInputHtmlType(gNxtObjArr);
            if (isAllDataEntered === false) {
                return;
            }
            // data is valid, reset timer by broadcasting RESET_TIMER event
            //$rootScope.$broadcast('RESET_TIMER');
            timerService.startTimer();

            // extract data and post into Earngo
            clipData.disableDivClipEntryTwo = true;
            clipData.divTwo.loading = true;

            /*saveAndFetchData(gNxtObjArr).then(function (clipsSetTwo) {
                clipData.clipsSetTwo = clipsSetTwo;
                clipData.divTwo.loading = false;
            });*/

            //save the clips data
            saveClipsData(gNxtObjArr);
            //clear the clips data
            clipData.clipsSetOne = [];
            //fetch the new clips data
            fetchClipsData(gNxtObjArr).then(function(result){
                clipData.clipsSetTwo = result;
                clipData.divTwo.loading = false;
            });

            clipData.disableDivClipEntryOne = false;
            clipData.showDivClipEntryOne = true;
            clipData.showDivClipEntryTwo = false;

            //add set focus
            $('#divClipEntryOne input').not('.unclear').first().focus();
        };


        //find first input Element to Focus next Div after submit
        /*this.firstInputElementOfNextDiv = function firstInputElementOfNextDiv(gNxtObjArr) {
            var nextObj = null;
            for (var i = 0; i < gNxtObjArr.length; i++) {
                nextObj = gNxtObjArr[i].clipUnclearCheckId;
                break;
            }
            return nextObj;
        }; */

        function saveAndFetchData(gNxtObjArr) {
            // submit the data
            restService.httpPostPromise(config.POST_IMAGE_RESULT, gNxtObjArr)
                .then(function (data) {
                    console.log('saved', data);
                });

            // fetch the new data 'service/getNextLargeImagesAfterSubmit'
            var promise = restService.httpPostPromise(config.URGENT_DATA_AFTER_SUBMIT, {SQS_NO: earngoStore.getQueueName()})
                .then(function (jsonArr) {
                    console.log('JSON ARRAY', jsonArr);
                    //largeClipDataVm.clipsSetOne =
                    if (!jsonArr || !jsonArr.imagesSetOne || jsonArr.imagesSetOne.length === 0) {
                        timerService.stopTimer();
                    }
                    return dataEntryUtilService.convertToJsObject(jsonArr.imagesSetOne);
                });
            return promise;
        }

        function saveClipsData(gNxtObjArr) {
            restService.httpPostPromise(config.POST_IMAGE_RESULT, gNxtObjArr)
                .then(function (data) {
                    console.log('Clips data saved', data);
                });
        }

        function fetchClipsData(gNxtObjArr) {
            // fetch the new data 'service/getNextLargeImagesAfterSubmit'
            var promise = restService.httpPostPromise(config.URGENT_DATA_AFTER_SUBMIT, {SQS_NO: earngoStore.getQueueName()})
                .then(function (jsonArr) {
                    console.log('JSON ARRAY', jsonArr);
                    //largeClipDataVm.clipsSetOne =
                    if (!jsonArr || !jsonArr.imagesSetOne || jsonArr.imagesSetOne.length === 0) {
                        timerService.stopTimer();
                    }
                    return dataEntryUtilService.convertToJsObject(jsonArr.imagesSetOne);
                });
            return promise;
        }

    }); //end of singin controller

