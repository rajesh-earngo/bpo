(function () {
    'use strict';

    function CheckerUrgentEntryController(checkerEntryCtrlFactory, config) {
        var ctrl = checkerEntryCtrlFactory.getInstance(config.CHK_URGENT_DATA_AFTER_SUBMIT, config.CHK_URGENT_DATA_ON_PAGE_LOAD);
        return ctrl
    }

    CheckerUrgentEntryController.$inject = ['checkerEntryCtrlFactory', 'config'];

    angular.module('app.modules.dataEntry.checker.urgent', ['ui.router', 'app.earngo.mock.backend', 'app.modules.dataentry.checker'])
        .controller('checkerEntryController', CheckerUrgentEntryController);
})();