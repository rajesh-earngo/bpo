//'use strict';
var largeEntryModule = angular.module('app.modules.dataEntry.large',
    ['ui.router', 'app.earngo.mock.backend', 'app.modules.timer', 'app.modules.dataentry.common.util']);

largeEntryModule.controller('largeEntryController', function ($scope, alertService, restService, config, timerService, dataEntryUtilService) {

    this.largeClipDataVm = {
        clipsSetOne: null,
        clipsSetTwo: null,
        showDivClipEntryOne: false,
        showDivClipEntryTwo: false,
        disableDivClipEntryOne: false,
        disableDivClipEntryTwo: false
    };

    var largeClipDataVm = this.largeClipDataVm;
    var selfCtrl = this;

    var PARENT_LARGE_DIV_ONE = 'divLargeEntryOne';
    var PARENT_LARGE_DIV_TWO = 'divLargeEntryTwo';

    // when signal to get new data is sent, get new set of data
    $scope.$on('GET_NEW_DATA_SET', function (evt) {
        selfCtrl.initialize();
    });

    this.initialize = function () {

        largeClipDataVm.disableDivClipEntryOne = false;
        largeClipDataVm.disableDivClipEntryTwo = false;
        largeClipDataVm.showDivClipEntryOne = true;
        largeClipDataVm.showDivClipEntryTwo = false;

        restService.httpPostPromise(config.LARGE_DATA_ON_PAGE_LOAD, {SQS_NO: earngoStore.getLargeQueueName()})
            .then(function (jsonArr) {
                if (!jsonArr || !jsonArr.imagesSetOne || jsonArr.imagesSetOne.length == 0) {
                    timerService.stopTimer();
                } else {
                    largeClipDataVm.clipsSetOne = dataEntryUtilService.convertToJsObject(jsonArr.imagesSetOne);
                    largeClipDataVm.clipsSetTwo = dataEntryUtilService.convertToJsObject(jsonArr.imagesSetTwo);
                }
            });
    };


    // Submit and Load data after Sumbit button is clicked for DIV One
    this.largeDataSubmit_One = function largeDataSubmit_One() {

        //var gNxtObjArr=getGlobalNextLargeImagesArrForDivOne();
        var gNxtObjArr = largeClipDataVm.clipsSetOne;

        // extract data and post into Earngo
        var isAllDataEntered = dataEntryUtilService.retrieveDataFromInputHtmlType(gNxtObjArr, '');
        if (isAllDataEntered == false) {
            return;
        }

        // data is valid, reset timer by broadcasting RESET_TIMER event
        //$rootScope.$broadcast('RESET_TIMER');
        timerService.resetTimer();

        //$("#divClipEntryOne *").prop('disabled', true);
        largeClipDataVm.disableDivClipEntryOne = true;

        //save and fetch new data
        saveAndFetchData(gNxtObjArr).then(function (clipsSetOne) {
            largeClipDataVm.clipsSetOne = clipsSetOne;
        });


        //switch between 2 divs hide and show
        largeClipDataVm.disableDivClipEntryTwo = false;
        largeClipDataVm.showDivClipEntryOne = false;
        largeClipDataVm.showDivClipEntryTwo = true;

        // focus on div two first input element which is not an "unclear" input
        $('#' + PARENT_LARGE_DIV_TWO + ' textarea').focus();
        $('#' + PARENT_LARGE_DIV_TWO + ' input').not('.unclear').first().focus();
    };


    /**
     * Submit large data and load data after submit
     * */
    this.largeDataSubmit_Two = function largeDataSubmit_Two() {

        var gNxtObjArr2 = largeClipDataVm.clipsSetTwo;
        // extract data and post into Earngo
        var isAllDataEntered = dataEntryUtilService.retrieveDataFromInputHtmlType(gNxtObjArr2);
        if (isAllDataEntered == false) {
            return;
        }
        // data is valid, reset timer by broadcasting RESET_TIMER event
        timerService.resetTimer();
        largeClipDataVm.disableDivClipEntryTwo = true;

        //save and fetch new data
        saveAndFetchData(gNxtObjArr2).then(function (clipsSetTwo) {
            largeClipDataVm.clipsSetTwo = clipsSetTwo;
        });

        largeClipDataVm.disableDivClipEntryOne = false;
        largeClipDataVm.showDivClipEntryOne = true;
        largeClipDataVm.showDivClipEntryTwo = false;

        // focus on div two first element (not unclear checkbox)
        $('#' + PARENT_LARGE_DIV_ONE + ' textarea').focus();
        $('#' + PARENT_LARGE_DIV_ONE + ' input').not('.unclear').first().focus();
    };


    function saveAndFetchData(gNxtObjArr) {
        // submit the data
        restService.httpPostPromise(config.POST_IMAGE_RESULT, gNxtObjArr)
            .then(function (data) {
                console.log('saved', data);
            });

        // fetch the new data 'service/getNextLargeImagesAfterSubmit'
        var promise = restService.httpPostPromise(config.LARGE_DATA_AFTER_SUBMIT, {SQS_NO: earngoStore.getLargeQueueName()})
            .then(function (jsonArr) {
                if (!jsonArr || !jsonArr.imagesSetOne || jsonArr.imagesSetOne.length == 0) {
                    timerService.stopTimer();
                }
                return dataEntryUtilService.convertToJsObject(jsonArr.imagesSetOne);
            });
        return promise;
    }

}); //end of  controller