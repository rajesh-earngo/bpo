var signinModule = angular.module('app.modules.signin', ['ui.router', 'app.earngo.mock.backend', 'ngFacebook', 'googleplus', 'app.modules.config', 'ngCookies']);


signinModule.config(['$facebookProvider', 'GooglePlusProvider', 'LOGIN_CONFIG', function ($facebookProvider, GooglePlusProvider, LOGIN_CONFIG) {
    $facebookProvider.setAppId(LOGIN_CONFIG.PRODUCTION.FACEBOOK_APP_ID).setPermissions(['email']);
    GooglePlusProvider.init({
        clientId: LOGIN_CONFIG.PRODUCTION.GOOGLE_APP_ID,
        apiKey: 'API_KEY'
    });

}]);

signinModule.run(function ($rootScope) {
    // Load the facebook SDK asynchronously
    (function () {
        // If we've already installed the SDK, we're done
        if (document.getElementById('facebook-jssdk')) {
            return;
        }

        // Get the first script element, which we'll use to find the parent node
        var firstScriptElement = document.getElementsByTagName('script')[0];

        // Create a new script element and set its id
        var facebookJS = document.createElement('script');
        facebookJS.id = 'facebook-jssdk';

        // Set the new script's source to the source of the Facebook JS SDK
        facebookJS.src = '//connect.facebook.net/en_US/all.js';

        // Insert the Facebook JS SDK into the DOM
        firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
    }());
});


signinModule.controller('signinController', ['$scope', '$location', '$cookies', '$state', 'restService', '$facebook', 'GooglePlus', 'config','alertService',
    function ($scope, $location, $cookies, $state, restService, $facebook, Googleplus, config, alertService) {


        $scope.$on('fb.auth.authResponseChange', function () {
            $scope.status = $facebook.isConnected();
            if ($scope.status) {
                $facebook.api(globalConfig.FACEBOOK_APP_ID).then(function (user) {
                    $scope.user = user;
                });
            }
        });

        $scope.facebookLogin = function () {

            console.log("Facebook login : " + $scope.status);

            if ($scope.status) {
                $facebook.logout();
            } else {
                $facebook.login().then(function () {
                    console.log("Welcome Facebook");
                    facebookEntryToEarngo();
                }).catch(
                    // Log the rejection reason
                    function (reason) {
                        console.log('Handle rejected promise (' + reason + ') here.');
                    });
            }
        };


        var facebookEntryToEarngo = function () {
            $facebook.api("/me").then(
                function (user) {
                    $scope.welcomeMsg = "Welcome " + user.name;

                    //enter into Earngo
                    var obj = {};
                    obj.facebookId = user.id;
                    obj.facebookName = user.name;

                    restService.httpPostPromise('service/fbSigninRestCall', user)
                        .then(function (result) {
                            if (result.RES_STATUS === 'S') {
                                //$('#headerLblUserName').text(obj.facebookName);
                                earngoStore.setEarngoId(result.EARNGO_ID);
                                earngoStore.setUserId(obj.facebookName);
                                earngoStore.setUserName(obj.facebookName);
                                earngoStore.setQueueName(obj.SQS_NO);
                                $location.path('/clipUrgentEntry');
                            } else {
                                $('#modal-alert').find('.modal-content p').text("Please verify username and password");
                                $('#modal-alert').openModal();
                                $location.path('/');
                            }
                        });
                },
                function (err) {
                    $scope.welcomeMsg = "Please log in";
                });
        };

        $scope.googleLogin = function () {
            Googleplus.login().then(function (authResult) {
                Googleplus.getUser().then(function (user) {
                    console.log("user : " + JSON.stringify(user));
                    //enter into Earngo
                    var obj = {};
                    obj.googleId = user.id;
                    obj.googleName = user.name;

                    restService.httpPostPromise('service/googleSigninRestCall', user)
                        .then(function (result) {
                            // console.log("message: " + JSON.stringify(data));
                            if (result.RES_STATUS === 'S') {
                                //$('#headerLblUserName').text(obj.googleName);
                                earngoStore.setEarngoId(result.EARNGO_ID);
                                earngoStore.setUserId(obj.googleName);
                                earngoStore.setUserName(obj.googleName);
                                earngoStore.setQueueName(obj.SQS_NO)
                                $location.path('/clipUrgentEntry');
                            } else {
                                $('#modal-alert').find('.modal-content p').text("Please verify username and password");
                                $('#modal-alert').openModal();
                                $location.path('/');
                            }
                        });

                });

            }, function (err) {
                console.log(err);
            });
        };

        // default as storage
        $scope.chkRememberMe = true;
        $scope.inputEmail = '';
        $scope.inputPassword = '';

        // device initialzation
        $scope.deviceInit = function deviceInit() {
            //clean if anything exist
            earngoStore.clearSessionToken();
            earngoStore.clearDeviceId();
            earngoStore.clearEarngoId();
            earngoStore.clearUserId();

            //set username and password from local storage
            if (earngoStore.getRememberMeId() !== 'undefined') {
                $scope.inputEmail = earngoStore.getRememberMeId();
                $scope.inputPassword = earngoStore.getRememberMePassword();
            }

            //call for device initialization
            restService.httpPostInit();
        };


        // User login validation
        $scope.userLogin = function userLogin() {

            var obj = {};
            obj.username = $scope.inputEmail;
            obj.password = $scope.inputPassword;
            obj.deviceId = earngoStore.getDeviceId();
            obj.sessionToken = earngoStore.getSessionToken();

            var rememberMe = $scope.chkRememberMe;
            console.log("rememberMe : " + rememberMe);

            restService.httpPostPromise(config.USER_LOGIN, obj) // 'service/signinRestCall'
                .then(function (result) {

                    if (result.RES_STATUS === 'S') {
                        earngoStore.setEarngoId(result.EARNGO_ID);
                        earngoStore.setUserId(obj.username);
                        earngoStore.setUserName(obj.username);
                        earngoStore.setCustomerId(result.CUST_SRNO);
                        earngoStore.setQueueName(result.SQS_NO);
                        earngoStore.setLargeQueueName(result.LARGE_SQS_NO);
                        earngoStore.setCheckerQueueName(result.CHECKER_SQS_NO);
                        //clear Rememberme
                        earngoStore.clearRememberMe();
                        if (rememberMe == true) {
                            //remember user and password in local storage
                            earngoStore.setRememberMeId(obj.username);
                            earngoStore.setRememberMePassword(obj.password);
                        }
                        // store the menus as part of menu access control
                        $cookies.putObject('topMenu', result.TOP_MENU);
                        $cookies.putObject('sideMenu', result.SIDE_MENU);
                        // loop through the top and side and
                        // redirect to the first state with
                        // "default" key == "Y"
                        for (var i = 0; i < result.TOP_MENU.length; i++) {
                            if (result.TOP_MENU[i].DEFAULT === 'Y') {
                                $state.go(result.TOP_MENU[i].STATE);
                            }
                        }

                        for (var j = 0; j < result.SIDE_MENU.length; j++) {
                            if (result.SIDE_MENU[j].DEFAULT === 'Y') {
                                $state.go(result.SIDE_MENU[j].STATE);
                            }
                        }
                    }

                    if (result.RES_STATUS === 'F'){
                        alertService.alert("Please verify username and password","Status");
                    }
                });
        }; // end of userlogin method


        // add a new task
        $scope.userSignout = function userSignout() {
            // alert(":Logout");
            $('#modal-alert').find('.modal-content p').text(":Logout");
            $('#modal-alert').openModal();
            earngoStore.clearSessionToken();
            earngoStore.clearDeviceId();
            earngoStore.clearEarngoId();

            $("#header_wrapper").hide();
            $location.path('/');
        };

        // Set timer to check if the input is filled with autocomplete data
        // if the input is filled with autocomplete, add active class to
        // its label to enable materialize styling
        // reference: https://github.com/Dogfalo/materialize/issues/194
        // http://stackoverflow.com/questions/11708092/detecting-browser-autofill
        var numOfAutocompleteInputs = $('input:-webkit-autofill').length;
        var checkNumOfAutocompleteInputs = window.setInterval(function () {
            if (numOfAutocompleteInputs !== $('input:-webkit-autofill').length) {
                clearInterval(checkNumOfAutocompleteInputs);
                $('input:-webkit-autofill ~ label').addClass('active');
            }
        }, 50);

    }]); //end of singin controller