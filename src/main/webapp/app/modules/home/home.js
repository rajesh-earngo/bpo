//'use strict';
var bodyModule = angular.module('app.modules.home', ['ui.router', 'app.earngo.mock.backend']);

/*Remove this ngroute dependency and replace with angular ui router*/
// bodyModule.config([ '$routeProvider', function($routeProvider) {
// 	$routeProvider.when('/dashboard', {
// 		templateUrl : 'pages/home/home.html',
// 		//controller : 'homeController'
// 	});
// } ]);

bodyModule.controller('homeController', function ($scope,$http,config) {
	
	console.log("home Controller");
	
	//get active tasks for BPO
	$scope.activeTaskForBPO = function activeTaskForBPO() {
		
		var obj=new Object(); 
		obj.deviceId=earngoStore.getDeviceId();
		obj.sessionToken=earngoStore.getSessionToken();	
		
		var req=$scope.buildHttpRequest (config.ACTIVE_TASK_COUNT,obj);
		
		$http(req)
		.success(function (data){
			console.log("active task of BPO : "+JSON.stringify(data));
			$('#spanUrgentDataEntry').text(data.TASK_COUNT_BPO);
			$('#spanLargeClips').text(data.TASK_COUNT_LARGE);
			 
			$('#spanTaskCountExceptionMatch').text(data.TASK_COUNT_EXCEPTION_MATCH);
			$('#spanTastCountChecker').text(data.TASK_COUNT_CHECKER);
			$('#spanTaskCountExceptionChecker').text(data.TASK_COUNT_EXCEPTION_CHECKER);
			$('#spanTaskCountExceptionDE').text(data.TASK_COUNT_EXCEPTION_DE);
			$('#spanFormsIdentified').text(data.IDENTIFY_SUCCESS);
			$('#spanFormsPending').text(data.IDENTIFY_PENDING);
			 
			 
		});
	};
	 
	// add a new task
	$scope.refresh = function refresh() {
		console.log("Enter login method.");
		//alert('Refresh');
		$scope.activeTaskForBPO();
	};
	
	
	//Use this method to create Http Request object
	$scope.buildHttpRequest = function buildHttpRequest(url,jsonReqObj) {
		console.log("Enter login method.");
		
		var req = {
				 method: 'POST',
				 url: url,
				 headers: {
				   'EARNGO_DEVICE_ID':earngoStore.getDeviceId(),
				   'EARNGO_SESSION_TOKEN' : earngoStore.getSessionToken(),
				   'EARNGO_ID' : earngoStore.getEarngoId()
				 },
				 data: JSON.stringify(jsonReqObj),
				};
		 
		return req;
	};
	
	
	//calling when page starts loading
	$scope.activeTaskForBPO();

}); //end of singin controller

