(function (angular) {

    'use strict';
    var matchFormModule = angular.module('app.modules.matchForm',
        ['ui.router', 'app.earngo.mock.backend', 'app.services.matchform']);


    /*Remove this ngroute dependency and replace with angular ui router*/
    // matchFormModule.config(['$routeProvider', function ($routeProvider) {
    //     $routeProvider.when('/matchform', {
    //         templateUrl: 'pages/failedform/match/match-form.html',
    //     });
    // }]);

    matchFormModule.controller('matchFormController', function ($scope, $translate, matchformModel, matchformDataService) {
        function MatchFormController() {

            this.init = function () {
                matchformDataService.init(matchformModel);
            };

            this.selectAvailableTemplate = function (custAvailTemplate) {
                matchformDataService.selectAvailableTemplate(custAvailTemplate, matchformModel);
            };

            this.saveMatch = function () {
                matchformDataService.saveMatchData(matchformModel);
            }

            this.saveDiscard = function () {
                matchformDataService.saveDiscardData(matchformModel);
            };

            this.getNext = function () {
                //check whether user has selected or discard
                if(matchformModel.isFormSelected == false) {
                    // alert('Either Match or Discard the form, before moving to Next.');
                    // Translation
                    $translate('MATCH_FORM.GET_NEXT_ERROR').then(function (translation) {
                        alert(translation);
                    });
                    return;
                }
                matchformDataService.getNextForm(matchformModel);
            };

            this.foundNewTemplate = function (){
                //$('#newTemplateModal').find('.modal-content p').text("Error in retrieving data");
                //$('#newTemplateModal').find('.modal-content p').text("Error in retrieving data");
                // matchformModel.statusLabel= 'Unidentified form is new template.'
                // $('#newTemplateModal').openModal();
                $translate('MATCH_FORM.FOUND_NEW_TEMPLATE').then(function (translation) {
                    matchformModel.statusLabel= translation;
                    $('#newTemplateModal').openModal();
                });
            }
        };

        $scope.vm = matchformModel;
        $scope.ctrl = new MatchFormController();

    });
})(angular);