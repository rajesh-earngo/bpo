(function (angular) {
  'use strict';
  var formEntryModule = angular.module('app.modules.formEntry',
    ['ui.router', 'app.earngo.mock.backend', 'app.services.formEntry']);


  /*Remove this ngroute dependency and replace with angular ui router*/
  // formEntryModule.config(['$routeProvider', function ($routeProvider) {
  //   $routeProvider.when('/formentry', {
  //     templateUrl: 'pages/failedform/entry/form-entry.html',
  //   });
  // }]);

  formEntryModule.controller('formEntryController', function ($scope, formEntryModel, formEntryDataService) {
    function FormEntryController() {

      this.init = function () {
        formEntryDataService.getNextForm(formEntryModel);
      };

      this.nextForm = function () {
        formEntryDataService.getNextForm(formEntryModel);
      };

      this.saveForm = function () {
        formEntryDataService.saveForm(formEntryModel);
      };
    };

    $scope.vm = formEntryModel;
    $scope.ctrl = new FormEntryController();

  });
})(angular);