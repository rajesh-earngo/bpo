/**
 * Created by Rajesh on 5/4/16.
 */
(function (angular) {
  'use strict';

  //matchform View Model
  function FormEntryModel() {
    this.custFormId = null;
    this.custFormPath = 'assets/img/no-image-available.png';
    this.fieldsArray = null;
  }

  FormEntryModel.prototype.test = function () {
    alert('alert');
  };


  //FormEntry Data Service
  var feDataService = function (restService, bpoConstants) {

    function init(formEntryModel) {
      getCustomerForm(formEntryModel);
    }

    function getNextForm(formEntryModel) {
      getCustomerForm(formEntryModel);
    }

    function setFormEntryModelData(resData, formEntryModel) {
      formEntryModel.custFormId = resData.custFormId;
      formEntryModel.custFormPath = resData.custFormPath;
      formEntryModel.fieldsArray = resData.fieldsArray;
    }

    function setDefaultModelData(formEntryModel) {
      formEntryModel.custFormId = null;
      formEntryModel.custFormPath = 'assets/img/no-image-available.png';
      formEntryModel.fieldsArray = null;
    }

    function saveForm(formEntryModel) {
      restService.httpPostPromise(bpoConstants.POST_FAILED_IMAGE_DETAIL, formEntryModel.fieldsArray)
        .then(function (resData) {
          console.log('match data :', resData);
          if (resData && resData.RES_STATUS === 'S') {
            alert('Submitted Successfully');
          }
        });
    }

    function getCustomerForm(formEntryModel) {
      restService.httpPostPromise(bpoConstants.FAILED_IMAGE_DETAIL, {})
        .then(function (resData) {
          console.log('match data :', resData);
          if (resData && resData.RES_STATUS === 'S') {
            setFormEntryModelData(resData, formEntryModel);
          } else {
            setDefaultModelData(formEntryModel);
          }
        });
    }


    return {
      init: init,
      getNextForm: getNextForm,
      saveForm: saveForm
    };
  };


  var module = angular.module('app.services.formEntry', []);
  module.service('formEntryDataService', feDataService);
  module.service('formEntryModel', FormEntryModel);

})(angular);