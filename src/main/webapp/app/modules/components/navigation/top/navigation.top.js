(function () {
    'use strict';
    var controller = function ($scope, $http, $state, $rootScope, $cookies, STATES_URLS) {
        var vm = this;

        vm.expandSideNav = function (clickEvent) {
            clickEvent.preventDefault();
            // broadcast event to expand sideNav
            $rootScope.$broadcast('SIDENAV_OPEN');
        };

        vm.setNavItems = function (menuItems) {
            // use the states in the menuItems to formulate
            // nav items
            var navItems = [];
            for (var i = 0; i < menuItems.length; i++) {
                // if the state is specified
                // then set it in the nav
                if (STATES_URLS[menuItems[i].STATE]) {
                    var item = STATES_URLS[menuItems[i].STATE];
                    item.state = menuItems[i].STATE;
                    navItems.push(item);
                }
            }

            // set the nav items
            vm.navItems = navItems;
        };
        // sort the topMenu items in ascending order 
        // and set them in navigation
        var items = $cookies.getObject('topMenu').sort(function (a, b) {
            return  a.SEQ_ORDER - b.SEQ_ORDER;
        });
        vm.setNavItems(items);

        // hide top menu on mobile via css, 
        // but show for certain states
        var showOnDesktopStates = [
            'main', 'main.forms', 'main.templates', 'main.quality', 'main.exception', 'main.profile', 'main.dashboard'
        ];

        if (showOnDesktopStates.indexOf($state.current.name) == -1) {
            vm.class = 'hide-on-med-and-down';
        }
    };

    // var linker = function (argument) {
    //     // body...
    // }


    function NavTopDirective() {
        var directive = {
            restrict: 'E',
            controller: controller,
            controllerAs: 'navCtrl',
            // link: linker,
            templateUrl: 'app/modules/components/navigation/top/navigation.top.directive.html'
        };
        return directive;
    }

    angular
        .module('app.modules.components.navigation.top', [])
        .directive('egnavigationtop', NavTopDirective);
})();