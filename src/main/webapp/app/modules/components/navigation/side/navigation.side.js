(function () {
    'use strict';


    var appendedNavItems = [
        {
            TITLE: "NAVIGATION.MENU_ITEM.HELP",
            STATE: "help",
            HREF: "",
            TOOLTIP: "NAVIGATION.TOOLTIPS.HELP",
            itemId: "liHelpId",
            linkId: "linkHelp",
        },
        {
            TITLE: "NAVIGATION.MENU_ITEM.TOUR",
            STATE: "tour",
            HREF: "",
            TOOLTIP: "NAVIGATION.TOOLTIPS.TOUR",
            itemId: "liTourId",
            linkId: "linkTour",
        }
    ];

    angular.module('app.modules.components.navigation.side', ['ngCookies']).directive('egnavigationside', ['$rootScope', '$cookies', function () {

        var controller = function ($scope, $http, $location, $rootScope, $state, $cookies, STATES_URLS, config) {

            var vm = this;

            vm.userPoints = 0;

            vm.userName= earngoStore.getUserName();

            vm.taskCounts = {
                linkUrgentClip: 0,
                liLargeId: 0,
                linkForm: 0,
                linkEntryForm: 0,
                linkCheckerClip: 0,
                linkCheckerLargeClip: 0
            };

            vm.state = $state.current.name;

            vm.getMenuClass = function () {
                // // collapse side menu on desktop for certain states 
                // // (also hide on mobile for these states), 
                // // but show on desktop for others
                // var collapseOnDesktopStates = [
                //     'main', 'main.forms', 'main.templates', 'main.quality', 'main.exception', 'main.profile', 'main.dashboard'
                // ];

                // if (collapseOnDesktopStates.indexOf($state.current.name) == -1) {
                //     return "fixed";
                // };
            };

            vm.getVisibility = function () {
                // only show the nav when it's not in the home page
                return $location.path() != '/';
            };

            vm.hideSideNav = function () {
                // need to programmatically close the sidenav
                // because when user clicks on sign out, the sidenav overlay is not hidden
                $('.button-collapse').sideNav('hide');
            };

            vm.getActiveMenuItemClass = function (path) {
                // using path key of navItems below, return active if the location is at the same path
                if ($location.path() == path) {
                    return 'active';
                }
            };

            // Define menu items
            vm.setNavItems = function (menuItems) {
                // set the nav items
                vm.navItems = menuItems.concat(appendedNavItems);;
            };

            // sort the sideMenu items in ascending order 
            // and set them in navigation
            var items = $cookies.getObject('sideMenu').sort(function (a, b) {
                return  a.SEQ_ORDER - b.SEQ_ORDER;
            });
            vm.setNavItems(items);

            vm.handleSignOutItem = function (linkId) {
                if (linkId == "linkSignOff") {
                    vm.hideSideNav();
                }
            };

            vm.handleHelpItem = function (state, $event) {
                if (state == 'help') {
                    // prevent route changing
                    $event.preventDefault();

                    // Broadcast a signal for modal to act on itself
                    $rootScope.$broadcast('HELP_MODAL_LAUNCH');
                }
            };

            vm.handleTourItem = function (state, $event) {
                if (state == 'tour') {
                    $event.preventDefault();
                    // launch tour slider gallery in popup
                    // reference: https://github.com/noelboss/featherlight/#content-filters
                    $.featherlightGallery($('<div data-featherlight="assets/img/tour-1.jpg"></div><div data-featherlight="assets/img/help.jpg"></div>'), {});
                }
            };

            // Fetch the number of task for UI to update
            vm.refreshTaskCount = function () {
                $scope.activeTaskForBPO();
            };

            //get active tasks for BPO
            $scope.activeTaskForBPO = function activeTaskForBPO() {

                var obj = {
                    deviceId: earngoStore.getDeviceId(),
                    sessionToken: earngoStore.getSessionToken()
                };

                var req = $scope.buildHttpRequest(config.ACTIVE_TASK_COUNT , obj);

                // set the number of tasks in the nav bar
                $http(req).success(function (data) {
                    var date = new Date();
                    var hours = date.getHours();
                    var minutes = (date.getMinutes() < 10) ? "0" + date.getMinutes() : date.getMinutes();
                    vm.taskCounts = {
                        urgent: data.TASK_COUNT_BPO || 0,
                        large: data.TASK_COUNT_LARGE || 0,
                        matchForm: data.TASK_COUNT_EXCEPTION_MATCH || 0,
                        formEntry: data.TASK_COUNT_EXCEPTION_DE || 0,
                        checkerUrgent: data.TASK_COUNT_CHECKER || 0,
                        checkerLarge: data.TASK_COUNT_LARGE_CHECKER || 0,
                        refreshTime: hours + ":" + minutes
                    };
                });
            };

            //Use this method to create Http Request object
            $scope.buildHttpRequest = function buildHttpRequest(url, jsonReqObj) {
                var req = {
                    method: 'POST',
                    url: url,
                    headers: {
                        'EARNGO_DEVICE_ID': earngoStore.getDeviceId(),
                        'EARNGO_SESSION_TOKEN': earngoStore.getSessionToken(),
                        'EARNGO_ID': earngoStore.getEarngoId()
                    },
                    data: JSON.stringify(jsonReqObj),
                };

                return req;
            };

            $scope.activeTaskForBPO();

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
                // Close all tooltips on state change
                $('.tooltipped').tooltip('remove');

                window.setTimeout(vm.hideSideNav, 300);
                // var isFromMain = fromState.name.startsWith('main.'),
                //     isToMain = toState.name.startsWith('main.');
                // if (isFromMain && !isToMain) {
                //     // close the sidenav to close the overlay
                // }
            });

            ///////////////////////
            // Get user's points //
            ///////////////////////
            $scope.getUserPoints = function getUserPoints() {
                var obj = {
                    deviceId: earngoStore.getDeviceId(),
                    sessionToken: earngoStore.getSessionToken()
                };

                var req = $scope.buildHttpRequest(config.FETCH_USER_POINTS, obj);

                $http(req).success(function (data) {

                    vm.userPoints = data.userPoints||0;
                });
            };

            $scope.getUserPoints();
        };

        var linker = function (scope, element, attrs, controller) {
            // collapse side menu on desktop for certain states 
            // (also hide on mobile for these states), 
            // but show on desktop for others
            var collapseOnDesktopStates = [
                'main', 'main.forms', 'main.templates', 'main.quality', 'main.exception', 'main.profile', 'main.dashboard'
            ];

            if (collapseOnDesktopStates.indexOf(controller.state) == -1) {
                // add fixed class to sidenav so that 
                // sidenav is fixed
                element[0].querySelector('#slide-out').className += " fixed";
            }

            // open the sidenav when signal is received (e.g. on navigation top toggle click)
            scope.$on('SIDENAV_OPEN', function (evt, args) {
                 $(element[0].querySelector('.button-collapse')).sideNav('show');
            });

            // init mobile collapse button
            $(element[0].querySelector('.button-collapse')).sideNav();
            scope.$watch(function getNumberOfNavItems() {
                return $(element).find('.tooltipped').length;
            }, function initTooltips() {
                $(element[0].querySelector('.button-collapse')).sideNav();
                $(element).find('.tooltipped').tooltip({delay: 50});
            });
        };

        return {
            restrict: 'E',
            controller: controller,
            controllerAs: 'navCtrl',
            // scope: {},
            link: linker,
            templateUrl: 'app/modules/components/navigation/side/navigation.side.directive.html'
        };
    }]);

})();