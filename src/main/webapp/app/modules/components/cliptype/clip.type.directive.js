/**
 * Created by Rajesh on 22/5/16.
 */
(function () {
    'use strict';

    function ClipController(scope, clipTypeEnum) {
        var markedZone = scope.templateModel.markedZone;

        this.clipTypeEnum = clipTypeEnum;

        this.numModel = {from: null, to: null};
        this.numAgree = function () {
            //get the values from selected
            console.log("numAgree", this.numModel);


            console.log("numAgree", scope.templateModel);
            markedZone.CLIP_TYPE = clipTypeEnum.NUMBER;
            markedZone.RANGE_VALUE_1 = this.numModel.from;
            markedZone.RANGE_VALUE_2 = this.numModel.to;
            ;

        };

        this.textModel = {textType:null};
        this.textAgree = function () {
            markedZone.CLIP_TYPE = this.textModel.textType;
        };

        this.dateModel = {dateType:null};
        this.dateAgree = function () {
            markedZone.CLIP_TYPE = this.dateModel.dateType;            
        };

        this.phoneAgree = function () {
            markedZone.CLIP_TYPE = this.clipTypeEnum.PHONE_NUMBER;           
        };

        this.snsAgree = function () {
            markedZone.CLIP_TYPE = this.clipTypeEnum.SNS;           
        };

        this.mcqModel = {mcqType:null, choices: null, rows: null, cols:null};
        this.mcqAgree = function () {
            markedZone.CLIP_TYPE = this.mcqModel.mcqType;       
        };
    }

    function ClipDirective() {
        var directive = {};

        directive.init = function () {
        };

        directive.restrict = 'EA';
        /* restrict this directive to elements */
        directive.templateUrl = null;
        directive.link = function ($scope, element, attrs) {
            directive.init();
        };
        directive.controller = ['$scope', 'clipTypeEnum', ClipController];
        directive.controllerAs = 'ctrl';

        directive.scope = {
            templateModel: "=ngModel"
        };
        return directive;

    }

    function ClipNumberDirective() {
        var directive = ClipDirective();
        directive.templateUrl = "app/modules/components/cliptype/clip.number.view.html";
        return directive;
    }

    function ClipTextDirective() {
        var directive = ClipDirective();
        /* restrict this directive to elements */
        directive.templateUrl = "app/modules/components/cliptype/clip.text.view.html";
        return directive;
    }

    function ClipDatetimeDirective() {
        var directive = ClipDirective();
        /* restrict this directive to elements */
        directive.templateUrl = "app/modules/components/cliptype/clip.datetime.view.html";
        return directive;
    }

    function ClipMcqDirective() {
        var directive = ClipDirective();
        /* restrict this directive to elements */
        directive.templateUrl = "app/modules/components/cliptype/clip.mcq.view.html";
        return directive;
    }

    function ClipPhoneDirective() {
        var directive = ClipDirective();
        /* restrict this directive to elements */
        directive.templateUrl = "app/modules/components/cliptype/clip.phone.view.html";
        return directive;
    }

    function ClipSnsDirective() {
        var directive = ClipDirective();
        /* restrict this directive to elements */
        directive.templateUrl = "app/modules/components/cliptype/clip.sns.view.html";
        return directive;
    }

    ClipNumberDirective.$inject = [];
    ClipTextDirective.$inject = [];
    ClipDatetimeDirective.$inject = [];
    ClipMcqDirective.$inject = [];
    ClipPhoneDirective.$inject = [];
    ClipSnsDirective.$inject = [];
    ClipController.$inject = ['clipTypeEnum'];
    angular
        .module('app.modules.components.clips', [])
        .controller('ClipController', ClipController)
        .directive('clipNumber', ClipNumberDirective)
        .directive('clipText', ClipTextDirective)
        .directive('clipMcq', ClipMcqDirective)
        .directive('clipDatetime', ClipDatetimeDirective)
        .directive('clipSns', ClipSnsDirective)
        .directive('clipPhone', ClipPhoneDirective);
})();