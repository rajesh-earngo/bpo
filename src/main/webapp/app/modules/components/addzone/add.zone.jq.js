(function () {
    'use strict';

    function AddZoneJqService() {

        this.feedCordsVar = function(){
            console.log('in AddZoneJqService.feedCordsVar');
            showLastCoord();
        }
        // addition of table field for tableUI

        this.feedCordsTableSingle = function(){
            var row = $('#choices_number_Single_row').val();
            var col = $('#choices_number_Single_col').val();
            showLastCoordTable(row, col);
        }
        // addition of table field for tableUI
        this.feedCordsTableMultiple = function(){
            var row = $('#choices_number_Mul_row').val();
            var col = $('#choices_number_Mul_col').val();
            showLastCoordTable(row, col);
        }
        this.loadBoxJQ = function(arraybox){

            delAll();
            var arr = arraybox;

            $.each(arr, function(index, val) {
                // addition of table field for tableUI
                if(val.CLIP_TYPE === 'MMT' || val.CLIP_TYPE === 'MST'){
                    for (var i = 0; i < val.CLIP_COORDINATE_X1.length; i++) {
                        var x = val.CLIP_COORDINATE_X1[i];
                        var y = val.CLIP_COORDINATE_Y1[i];
                        var w = val.CLIP_COORDINATE_X2[i] - val.CLIP_COORDINATE_X1[i];
                        var h = val.CLIP_COORDINATE_Y2[i] - val.CLIP_COORDINATE_Y1[i];
                        var seq = val.CLIP_SEQ_NO;
                        addRect(x, y, w ,h , 'rgba(128, 203, 196, 0.6)',seq);
                    }
                }else{
                    var x = val.CLIP_COORDINATE_X1;
                    var y = val.CLIP_COORDINATE_Y1;
                    var w = val.CLIP_COORDINATE_X2 - val.CLIP_COORDINATE_X1;
                    var h = val.CLIP_COORDINATE_Y2 - val.CLIP_COORDINATE_Y1;
                    var seq = val.CLIP_SEQ_NO;
                    addRect(x, y, w ,h , 'rgba(128, 203, 196, 0.6)',seq);
                }
            });
        }

        this.loadJQ = function () {
            // $('select').material_select();
            /*alertService.alert('Hello','Title');*/
            console.log("Load Templates Jquery");

            //del for table UI
            $('#tmp-tabledel-rec').click(function() {
                var type = $('input[name="typeMcqRad"]:checked').val();
                if( type == 'MST' ){
                    console.log('in single');
                    var row = $('#choices_number_Single_row').val();
                    var col = $('#choices_number_Single_col').val();
                    var total = ((row*col)+1); //+1 for main rectangle
                    for (var i = 0; i < total; i++) {
                        delRec();
                    }
                }else if( type == 'MMT' ){
                    console.log('in multiple');
                    var row = $('#choices_number_Mul_row').val();
                    var col = $('#choices_number_Mul_col').val();
                    var total = ((row*col)+1); //+1 for main rectangle
                    for (var i = 0; i < total; i++) {
                        delRec();
                    }
                }else{
                    return;
                }
            });

            $('.rightMenuHam p').click(function () {
                $('.rightmenu').css('right', '0px');
            });

            $('.rightHead').click(function () {
                $('.rightmenu').css('right', '-300px');
            });

            $('.addZoneBtn').click(function () {
                $('.rightmenu').css('right', '-300px');
            });

            $('.editModalBtn').click(function() {
/*                backCanvasFn();*/
                $('#editModal').openModal();
            });

            $('.closeEditModal').click(function() {
                $('#editModal').closeModal();
            });

/*            $('body').on('click', '#remEditTr', function() {
                console.log('m in remEditTr');
                var ptr = $(this).parents('tr');
                console.log(ptr);
                var i = $("table.editTable tr").index($(ptr));
                console.log(i);
                delAll();
                boxes2.splice((i-1), 1);
                $(this).parents('tr').fadeOut('slow');
                reDrawAfterDel();
            });*/

/*            $('body').on('click', '.idenRow', function() {
                var ptr = $(this).parents('tr');
                var index = $("table.editTable tr").index($(ptr));
                console.log("tr number" + index);
            });*/
        };
        return this;
    }


    AddZoneJqService.$inject = ['alertService'];

    angular
        .module('app.modules.components.addZone.jq.service', [])
        .service('addZoneJqService', AddZoneJqService);

})();