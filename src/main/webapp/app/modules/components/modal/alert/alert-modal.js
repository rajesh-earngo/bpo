(function () {

    angular.module('app.modules.components.modal.alert', []).directive('egalertmodal', ['$rootScope', function ($rootScope) {
        var earngoDefaults = {
            dismissible: false, // Modal can be dismissed by clicking outside of the modal
            opacity: 0.5, // Opacity of modal background
            in_duration: 300, // Transition in duration
            out_duration: 200, // Transition out duration
            ready: function () {
            },
            complete: function () {
                // broadcast signal that the modal is closed so other directives,
                // e.g. timer can pick up
                $rootScope.$broadcast('ALERT_MODAL_CLOSED');
            }
        };

        var controller = function ($scope) {
            var vm = this;
            vm.text = "Default text";
            console.log("this is modal text controller");
        };

        var linker = function (scope, element, attrs) {
            var options = $.extend(true, {}, earngoDefaults, scope.$eval(attrs.modal));
            var modalElement = $(element).find('#modal-alert');
            // launch the modal when signal is received (e.g. on Help nav item click)
            scope.$on('ALERT_MODAL_LAUNCH', function (evt, args) {
                modalElement.openModal(options);
            });
        };

        return {
            restrict: 'E',
            controller: controller,
            controllerAs: 'alertCtrl',
            link: linker,
            templateUrl: 'app/modules/components/modal/alert/alert-modal.html'
        };
    }]);

})();