(function () {

angular.module('app.modules.components.modal.help', []).directive('eghelpmodal', ['$rootScope', function ($rootScope) {
    var earngoDefaults = {
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: 0.5, // Opacity of modal background
        in_duration: 300, // Transition in duration
        out_duration: 200, // Transition out duration
        ready: function() {
        },
        complete: function() { 
            // broadcast signal that the modal is closed so other directives, 
            // e.g. timer can pick up
            $rootScope.$broadcast('HELP_MODAL_CLOSED');
        }
    };

    var linker = function (scope, element, attrs) {
        var options = $.extend(true, {}, earngoDefaults, scope.$eval(attrs.modal));
        var modalElement = $(element).find('#modal-help');
        // launch the modal when signal is received (e.g. on Help nav item click)
        scope.$on('HELP_MODAL_LAUNCH', function (evt) {
            modalElement.openModal(options);
        });
    };

    return {
        restrict: 'E',
        link: linker,
        templateUrl: 'app/modules/components/modal/help/help-modal.html'
    };
}]);

})();