(function () {

angular.module('app.modules.components.modal.timeout', []).directive('egtimeoutmodal', ['$rootScope', function ($rootScope) {
    var earngoDefaults = {
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: 0.5, // Opacity of modal background
        in_duration: 300, // Transition in duration
        out_duration: 200, // Transition out duration
        ready: function() {
        },
        complete: function() { 
            // time out modal is closed, send signal to get new data set
            // broadcast signal that the modal is closed so other directives, 
            // e.g. timer can pick up
            $rootScope.$broadcast('GET_NEW_DATA_SET');
            // send signal to reset timer
            $rootScope.$broadcast('RESET_TIMER');
        }
    };

    var linker = function (scope, element, attrs) {
        var options = $.extend(true, {}, earngoDefaults, scope.$eval(attrs.modal));
        var modalElement = $(element).find('#timeout');
        // when the timer times out, launch the modal
        scope.$on('OPEN_MODAL', function (evt) {
            modalElement.openModal(options);
        });
    };

    return {
        restrict: 'E',
        link: linker,
        templateUrl: 'app/modules/components/modal/timeout/timeout-modal.html'
    };
}]);

})();