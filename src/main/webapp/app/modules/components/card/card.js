(function () {

angular.module('app.modules.components.card', []).directive('egcard', function (clipTypeEnum) {

    var controller = function ($scope, $state, $timeout, CLIP_TITLES) {
        // this = ctrl which is available on $scope
        var vm = this;

        /////////////////////////
        // Current state / url //
        /////////////////////////
        vm.state = $state.current.name;

        ////////////////////////////////
        // Whether the card is active //
        ////////////////////////////////
        vm.isActive = false;

        vm.setActive = function () {
            vm.isActive = true;
        };

        vm.setInactive = function () {
            vm.isActive = false;
        };

        /////////////////////////////////////////////////////
        // Set card title text and orientation for display //
        /////////////////////////////////////////////////////
        for (var key in clipTypeEnum) {
            if ($scope.card.clipType == clipTypeEnum[key]) {
                $scope.card.title = CLIP_TITLES[key];
                $scope.card.tooltipMsg = CLIP_TITLES[key].replace('TITLE','TOOLTIPS');

                $scope.card.orientation = 'horizontal';
                if (key == 'SINGLE_SELECT_VERT' || key == 'MULTI_SELECT_VERT' || key == 'LARGE_MCQ') {
                    $scope.card.orientation = 'vertical';
                }
                break; // stop the loop
            }
        }

        ///////////////////////////////////////////
        // Prepare for loading of input partials //
        ///////////////////////////////////////////
        vm.getInputsPartialPath = function (clipType) {
            // load different set of inputs for checker
            if ( $state.current.name == 'checkerUrgent' || $state.current.name == 'checkerLarge' ) {
                return 'app/modules/components/card/checker/partials/inputs/'+clipType+'.html';
            }
            return 'app/modules/components/card/partials/inputs/'+clipType+'.html';
        };

        ////////////////////////////
        // Handle keypress events //
        ////////////////////////////
        vm.onKeyPress = function ($event) {

            var card = $($event.currentTarget);

            //////////////////////////////////
            // Shift + Tab -> Previous clip //
            //////////////////////////////////
            // shift + tab pressed, navigate to the previous cards
            if ($event.shiftKey && $event.keyCode == 9) {

                var prevElement = card.prev();

                if (prevElement.hasClass('card')) {
                    $event.preventDefault(); // prevent default tab focusing
                    prevElement.find('input').not('.unclear').first() // then find the first input that is not an "unclear" input,
                        .focus(); // and focus on that input

                    // stop the chain of event listeners
                    return;
                }
            }

            //////////////////////
            // Tab -> Next clip //
            //////////////////////
            // tab (9) key pressed (without shift), navigate to the next card or submit button
            if ($event.keyCode === 9 && !$event.shiftKey) {
                $event.preventDefault(); // prevent default tab focusing

                var nextElement = card.next();

                if (nextElement.hasClass('card')) {
                    nextElement.find('textarea').focus();
                    if (nextElement.find('textarea').length === 0) {
                        nextElement.find('input').not('.unclear').first() // then find the first input that is not an "unclear" input,
                            .focus(); // and focus on that input
                    }
                    return;
                }
                else {
                    // focus on the submit button
                    nextElement.find('button').focus();
                    return;
                }   
            }

            // find all the inputs in the card
            var inputs = $(event.currentTarget).find('input');

            ////////////////////////////////////////////
            // Right arrow / Down arrow -> Next input //
            ////////////////////////////////////////////
            // right (39) or down (40) arrow key pressed, navigate to next input
            if ($event.keyCode === 39 || $event.keyCode === 40) {
                $event.preventDefault(); // prevent default tab focusing
                // find all the inputs in the card
                for (var i = 0; i < inputs.length; i++) {
                    // find the input with the focus
                    if ($(inputs[i]).is(':focus')) {
                        // if this is the last input
                        if (i == inputs.length - 1) {
                            // focus the first input
                            $(inputs[0]).focus();
                            return;
                            // break;
                        }
                        // otherwise focus the next input
                        $(inputs[i + 1]).focus();
                        return;
                        // break;
                    }
                }
            }

            /////////////////////////////////////////////
            // Left arrow / Up arrow -> Previous input //
            /////////////////////////////////////////////
            // left (37) or up (38) arrow key pressed, navigate to previous input
            if ($event.keyCode === 37 || $event.keyCode === 38) {
                $event.preventDefault(); // prevent default tab focusing
                for (var j= 0; j< inputs.length; j++) {
                    // find the input with the focus
                    if ($(inputs[j]).is(':focus')) {
                        // if this is the first input
                        if (j=== 0) {
                            // focus the last input
                            $(inputs[inputs.length - 1]).focus();
                            return;
                            // break;
                        }
                        // otherwise focus the previous input
                        $(inputs[j- 1]).focus();
                        return;
                        // break;
                    }
                }
            }

            /////////////////////////////////
            // Alt + U = Toggle "unclear" //
            /////////////////////////////////
            // u = key code 85
            if ($event.altKey && $event.keyCode == 85) {
                $event.preventDefault(); // prevent chrome and firefox shortcut browser conflicts
                toggleAndFocusInput(card.find('.unclear'));
                // update the unclear data model
                vm.isUnclear = !vm.isUnclear;
                    
                // stop the chain of event listeners
                return;
            }

            /////////////////////////////////
            // alt + Z = ZoomOut or Zoomin //
            /////////////////////////////////
            // z = key code 90
            if ($event.altKey && $event.keyCode == 90) {
                $event.preventDefault(); // prevent chrome and firefox shortcut browser conflicts
                var imageId= "#" + $scope.card.clipImgId;
                if($( imageId ).hasClass( "zoom-out" )){
                    $(imageId).removeClass( "zoom-out" );
                }else{
                    $(imageId).addClass( "zoom-out" );
                }
                // stop the chain of event listeners
                return;
            }

            /////////////////////////////////////////////////
            // Pressing a number toggles the N-th input    //
            // NOTE: For checker urgent and checker large, //
            //       we need to skip 1 input as the first  //
            //       input is the correct checkbox         //
            /////////////////////////////////////////////////
            // only for checkbox and radio inputs
            // and when focused input is not a text box
            var $focusedInput = $(':focus');
            if (
                $focusedInput.attr('type') !== 'text' &&
                (
                    $scope.card.clipType == clipTypeEnum.SINGLE_SELECT_VERT ||
                    $scope.card.clipType == clipTypeEnum.SINGLE_SELECT_HOR ||
                    $scope.card.clipType == clipTypeEnum.MULTI_SELECT_VERT ||
                    $scope.card.clipType == clipTypeEnum.MULTI_SELECT_HOR ||
                    $scope.card.clipType == clipTypeEnum.SNS ||
                    $scope.card.clipType == clipTypeEnum.MALE_FEMALE ||
                    $scope.card.clipType == clipTypeEnum.YES_NO ||
                    $scope.card.clipType == clipTypeEnum.LARGE_MCQ
                )
                ) {

                // 1-9 = key code 49-57 or 1-9 = key codes 96 to 105
                if (($event.keyCode >= 49 && $event.keyCode <= 57) || ($event.keyCode >= 96 && $event.keyCode <= 105)) {
                    // Figure out which N-th input should be toggled, 0 = first
                    var index = $event.keyCode - 49;
                    if($event.keyCode > 57 ){
                        index = $event.keyCode - 96;
                    }
                    var inputsWithoutUnclear = inputs.not('.unclear');
                    // Adding 1 here to skip the "correct" input
                    // for checker urgent and large states
                    if ( $state.current.name == 'checkerUrgent' || $state.current.name == 'checkerLarge' ) {
                        index++;
                    }
                    toggleAndFocusInput($(inputsWithoutUnclear[index]));
                    // stop the chain of event listeners
                    return;
                }
            }

            //////////////////////////////
            // Y or N toggles Yes or No //
            //////////////////////////////
            // Y = 89, N = 78
            if ($scope.card.clipType == clipTypeEnum.YES_NO) {
                // Y pressed
                if ($event.keyCode == 89) {
                    // Toggle Yes input and focus it
                    toggleAndFocusInput(card.find('input[value="Y"]'));
                    
                    // stop the chain of event listeners
                    return;
                }

                // N pressed
                if ($event.keyCode == 78) {
                    // Toggle No input and focus it
                    toggleAndFocusInput(card.find('input[value="N"]'));
                    
                    // stop the chain of event listeners
                    return;
                }
            }

            /////////////////////////////////////////////
            // S or N toggles Selected or Non-Selected //
            /////////////////////////////////////////////
            // S = 83, N = 78
            if ($scope.card.clipType == clipTypeEnum.SNS) {
                // Y pressed
                if ($event.keyCode == 83) {
                    // Toggle Yes input and focus it
                    toggleAndFocusInput(card.find('input[value="S"]'));
                    
                    // stop the chain of event listeners
                    return;
                }

                // N pressed
                if ($event.keyCode == 78) {
                    // Toggle No input and focus it
                    toggleAndFocusInput(card.find('input[value="NS"]'));
                    
                    // stop the chain of event listeners
                    return;
                }
            }

            ///////////////////////////////////
            // M or F toggles Male or Female //
            ///////////////////////////////////
            // M = 77, F = 70
            if ($scope.card.clipType == clipTypeEnum.MALE_FEMALE) {
                // Y pressed
                if ($event.keyCode == 77) {
                    // Toggle Yes input and focus it
                    toggleAndFocusInput(card.find('input[value="M"]'));

                    // stop the chain of event listeners
                    return;
                }

                // N pressed
                if ($event.keyCode == 70) {
                    // Toggle No input and focus it
                    toggleAndFocusInput(card.find('input[value="F"]'));
                    
                    // stop the chain of event listeners
                    return;
                }
            }

            /////////////////////////////////////
            // Ctrl + Enter submits all inputs //
            /////////////////////////////////////            
            // Enter = 13
            if ($event.ctrlKey && $event.keyCode == 13) {

                var submitButton = card.siblings('.fixed-action-btn').find('button');

                // click the button without conflicting with the current angular digest cycle
                // reference: http://stackoverflow.com/questions/29008322/apply-already-in-progress-when-sending-a-click-event-to-a-button
                $timeout(function () {
                    submitButton.click();
                });
                    
                // stop the chain of event listeners
                return;
            }
        };

        function toggleAndFocusInput (input) {
            var inputStatus = input.prop('checked');
            input.prop('checked', !inputStatus).focus();
        }

        /////////////////////////////////////
        // Handle whether image is unclear //
        /////////////////////////////////////
        vm.isUnclear = false;

        vm.getUnclearStatus = function () {
            return vm.isUnclear;
        };

        //////////////////////////////////////
        // Handle whether image is overflow //
        //////////////////////////////////////
        vm.isOverflow = false;

        ////////////////////////////////////
        // Handle showing of help message //
        ////////////////////////////////////
        vm.isShowingHelp = false;

        vm.getShowHelpStatus = function () {
            return vm.isShowingHelp;
        };

        vm.showHelp = function () {
            vm.isShowingHelp = true;
        };

        vm.hideHelp = function () {
            vm.isShowingHelp = false;
        };

        ///////////////////////////////////
        // Handle rotation of image clip //
        ///////////////////////////////////
        vm.image = {
            angle: 0 // no rotation
        };

        vm.rotateClockwise = function () {
            vm.image.angle = (vm.image.angle + 90 == 360) ? 0 : vm.image.angle += 90;
        };

        vm.rotateAntiClockwise = function () {
            vm.image.angle = (vm.image.angle - 90 == -90) ? 270 : vm.image.angle -= 90;
        };

        vm.getRotation = function () {
            var rotation = vm.image.angle % 360;
            return rotation;
        };

        ////////////////////////////////////////////
        // Prevent input from exceeding maxlength //
        ////////////////////////////////////////////
        vm.date = null;
        vm.datemaxlength = 8;
        $scope.$watch(function watchDateMaxlength (scope) {
            return (vm.date);
        }, function handleDateMaxlength (newValue, oldValue) {
            if (newValue && newValue.toString().length > vm.datemaxlength) {
                vm.date = parseInt(newValue.toString().substring(0, vm.datemaxlength));
            }
        });

        vm.time = null;
        vm.timemaxlength = 6;
        $scope.$watch(function watchTimeMaxlength (scope) {
            return (vm.time);
        }, function handleTimeMaxlength (newValue, oldValue) {
            if (newValue && newValue.toString().length > vm.timemaxlength) {
                vm.time = parseInt(newValue.toString().substring(0, vm.timemaxlength));
            }
        });
    };

    var linker = function (scope, element, attrs, controller) {
        // load the horizontal or vertical card template
        scope.getTemplateUrl = function () {
            // load different set of inputs for checker
            if ( controller.state == 'checkerUrgent' || controller.state == 'checkerLarge' ) {
                return 'app/modules/components/card/checker/card-' + scope.card.orientation + '.html';
            }
            return 'app/modules/components/card/card-' + scope.card.orientation + '.html';
        };

        // Trigger when number of inputs in the element changes,
        // including by directives like ng-repeat
        // reference: http://stackoverflow.com/questions/12304291/angularjs-how-to-run-additional-code-after-angularjs-has-rendered-a-template
        var focusWatch = scope.$watch(function() {
            return element.find('input').length;
        }, function() {
            // Wait for templates to render
            scope.$evalAsync(function() {
                // Finally, directives are evaluated
                // and templates are rendered here
                // scope.cardCtrl.index is set using ng-init to 
                // detect if it's the first card in the ng-repeat
                // reference: http://stackoverflow.com/questions/14807258/access-index-of-the-parent-ng-repeat-from-child-ng-repeat
                // if the card is the first element
                if (scope.cardCtrl.index === 0) {
                    // find its first input that's not 
                    // an unclear checkbox and focus it
                    // focus textarea first for large text clips
                    $(element).find('textarea').focus();
                    // then focus non textareas, which will do nothing if the clip 
                    // is a large text clip, hence leaving textarea focused
                    $(element).find('input').not('.unclear').first().focus();
                }
            });
        });

        var tooltipWatch = scope.$watch(function () {
            return $(element).find('.tooltipped').length;
        }, function () {
            $(element).find('.tooltipped').tooltip({delay: 50});
        });
    };
    
    return {
        restrict: 'E',
        controller: controller,
        controllerAs: 'cardCtrl',
        link: linker,
        scope: true,
        template: '<div ng-include="getTemplateUrl()"></div>'
    };
});

})();