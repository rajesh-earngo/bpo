/**
 * Created by Rajesh on 21/5/16.
 */
(function () {
  'use strict';

  function SelectTemplate(selectTemplateJqService, alertService) {

    function SelectFormController($scope) {

      var ctrl = {};
      var parentCtrl = $scope.$parent.templatesCtrl;

      ctrl.formSelected = function (page) {
        /*
         parentCtrl.allowZoneMarking = true;
         parentCtrl.vm.allowZoneMarking = true;*/

        parentCtrl.vm.canvasImagePath = page.pageUrl;
        parentCtrl.vm.template.TEMPLATE_ID = page.pageId;
        selectTemplateJqService.initBoxLoad();

        parentCtrl.fetchROI(page);
      };

      ctrl.collapsibleInner = function () {
        $('.selectFormcollapsibleInner').collapsible({
          accordion: false
        });
      };

      return ctrl;
    }

    var directive = {};
    directive.init = function () {
      selectTemplateJqService.loadJQ();
    };

    directive.scope = {
      formDetailsModel: '=',
      formSelected: '&'
    };
    directive.restrict = 'EA';
    directive.controller = ['$scope', SelectFormController];
    directive.controllerAs = 'selectFormCtrl';
    /* restrict this directive to elements */
    directive.templateUrl = "app/modules/components/selectform/select.form.view.html";
    directive.link = function ($scope, element, attrs) {
      directive.init();
    };

    return directive;
  }

  SelectTemplate.$inject = ['selectTemplateJqService', 'alertService'];
  angular
    .module('app.modules.components.selectForm', ['app.modules.components.selectForm.jq.service'])
    .directive('selectTemplate', SelectTemplate);
})();