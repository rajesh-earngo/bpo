(function (angular) {

    var serviceRedeem = angular.module('app.services.redeem', []);

    serviceRedeem.service('redeemService', function () {

        var isProfileUpdateBeforeRedeem = false;

        function getProfileUpdateStatus() {
            return isProfileUpdateBeforeRedeem;
        }


        function setProfileUpdateStatus(status) {
            isProfileUpdateBeforeRedeem = status;
        }

        return {
            getProfileUpdateStatus: getProfileUpdateStatus,
            setProfileUpdateStatus: setProfileUpdateStatus
        }
    });

})(angular);