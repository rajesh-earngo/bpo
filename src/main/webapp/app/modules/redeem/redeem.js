'use strict';
var redeemModule = angular.module('app.modules.redeem', ['ui.router', 'app.earngo.mock.backend', 'app.services.redeem', 'app.services.userpoints']);

/*Remove this ngroute dependency and replace with angular ui router*/
// redeemModule.config(['$routeProvider', function ($routeProvider) {
//     $routeProvider.when('/redeem', {
//         templateUrl: 'pages/redeem/redeem.html',
//     });
// }]);

redeemModule.controller('redeemController', function ($scope, $location, restService, redeemService, userPointsService) {


    $scope.availableEarngoPoints = 0;
    $scope.redeemPoints = "";
    $scope.balancePoints = 0;
    $scope.isRedeemBttnActivate = false;

    $scope.initialize = function () {

        if (redeemService.getProfileUpdateStatus() == true) {
            $scope.isRedeemBttnActivate = true;
        }

        //get the userpoints
        userPointsService.getUserPoints().then(function (points) {
            $scope.availableEarngoPoints = points;
        });
    }


    $scope.profileVerifyAction = function () {
        $location.path('/profile');
    }

    $scope.redeemPointsAction = function () {
        var reqObj = {};
        reqObj.POINTS_REDEEM = $scope.redeemPoints || 0;

        if (parseInt(reqObj.POINTS_REDEEM) < 10) {
            showModalAlert('Redeem points must be more than 10.');
            return false;
        }

        restService.httpPost('service/redeemPoints', reqObj, function (data, status) {
            var status = data.RES_STATUS;
            if (status === 'S') {
                $scope.balancePoints = parseInt($scope.availableEarngoPoints) - parseInt($scope.redeemPoints);
                showModalAlert('Points Redeemed.');
            } else {
                showModalAlert(data.RES_MESSAGE);
            }
        });
    };

    $scope.cancelAction = function () {
        $location.path('/clipUrgentEntry');
    }

    function showModalAlert(msg) {
        $('#modal-alert').find('.modal-content p').text(msg);
        $('#modal-alert').openModal();
        $('#modal-alert-bttn-ok').focus();
    }

});