/**
 * Created by Rajesh on 16/4/16.
 */
(function () {
    'use strict';

    function LoginService($location, restService, config) {

        this.login = function (loginModel) {
            restService.httpPostPromise(config.VALIDATE_CUSTOMER, loginModel)
                .then(function (result) {
                    console.log("login ", result);
                    var status = result.RES_STATUS;

                    if (status === 'S') {
                        //$('#headerLblUserName').text(obj.facebookName);
                        earngoStore.setEarngoId(result.CUST_USER_ID);
                        earngoStore.setUserId(result.CUST_USER_ID);
                        earngoStore.setUserName(loginModel.username);
                        earngoStore.setCustomerId(result.CUST_USER_ID)
                        $location.path('/main/dashboard');
                    } else {
                        //alert("Login Failed");
                        loginModel.alerts.push({'ERROR':'Username Or Password is invalid.'});
                        $location.path('/');
                    }
                });
        };

        this.deviceInit = function () {
            restService.httpPostInit();
        };

        return this;
    }

    function LoginModel() {
        this.username = null;
        this.password = null;
        this.alerts = [];
    }

    LoginModel.$inject = [];
    LoginService.$inject = ['$location', 'restService', 'config'];

    angular.module('app.modules.login.service', [])
        .service('loginService', LoginService)
        .service('loginModel', LoginModel);

})();