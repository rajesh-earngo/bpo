(function () {
    'use strict';


    function ExceptionController($scope) {

        var ctrl = this;

        ctrl.DoCtrlPagingAct = function(text, page, pageSize, total) {
            console.log(text, page, pageSize, total );
        };

        ctrl.init = function () {
            console.log('Controller exception');
            this.total = 100;
        };

        // ctrl.vm = loginModel;

        // ctrl.init = function () {
        //     loginService.deviceInit();
        // };
    }

    ExceptionController.$inject = [];

    angular
        .module('app.modules.exception', ['bw.paging'])
        .controller('exceptionController', ExceptionController);

})();