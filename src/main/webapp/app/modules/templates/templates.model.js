/**
 * Created by Rajesh on 16/5/16.
 */
(function () {
  'use strict';

  function TemplatesModelFactory(DEFAULT) {

    var factory = this;

    function TemplatesModel() {

      this.template = new Template();
      this.markedZone = new MarkedZone();
      this.canvasImagePath = null;
      this.templateList = null;
      this.customerId = earngoStore.getCustomerId();
      this.allowZoneMarking = false;
      this.allowFieldInput = false;

    }

    TemplatesModel.prototype.getTemplate = function () {
      return this.template;
    };

    TemplatesModel.prototype.pushMarkedZone = function () {
      this.template.addMarkedZone(this.markedZone)
    };

    TemplatesModel.prototype.clearMarkedZone = function () {
      for (var p in this.markedZone) {
        if (this.markedZone.hasOwnProperty(p))
          this.markedZone [p] = null;
      }
     this.setDefaultMarkedZone();
    };

    TemplatesModel.prototype.getMarkedZone = function () {
      return this.markedZone;
    };

    TemplatesModel.prototype.setDefaultMarkedZone = function () {
      //set default value for requied field
      this.markedZone.CLIP_QUALITY = DEFAULT.QUALITY_TYPE_VALUE;
      this.markedZone.CLIP_SLA = DEFAULT.SLA_TYPE_VALUE;
      this.markedZone.CLIP_MANDATORY = DEFAULT.CLIP_MANDATORY;
      this.markedZone.CLIP_SEQ_NO = DEFAULT.CLIP_SEQ_NO;
      this.markedZone.CLIP_SUB_SEQ_NO = DEFAULT.CLIP_SUB_SEQ_NO;
      this.markedZone.LANGUAGE_SRNO = DEFAULT.LANGUAGE_SRNO;
      if(this.template.markedZoneArray && this.template.markedZoneArray.length > 0){
        this.markedZone.CLIP_SEQ_NO = this.template.markedZoneArray.length+1;
      }
    };

    function Template() {
      this.CUST_SRNO = earngoStore.getCustomerId();
      this.TEMPLATE_ID = null;
      this.FORM_ID = null;
      this.FORM_NAME = null;
      this.MP = null;
      this.PAGE_LINKAGE = null;
      this.RES_CODE = null;
      this.RES_IMAGE_HEIGHT = null;
      this.RES_IMAGE_PATH = null;
      this.RES_IMAGE_WIDTH = null;
      this.RES_MESSAGE = null;
      this.RES_STATUS = null;
      this.TOTAL_ZONE_MARKINGS = null;
      this.markedZoneArray = []; //refer to MarkedZone[]
    }

    Template.prototype.addMarkedZone = function (markedZone) {
      var copyObj = Object.keys(markedZone).reduce(function (c, k) {
        c[k] = markedZone[k];
        return c;
      }, {});
      this.markedZoneArray.push(copyObj);
    };

    function MarkedZone() {
      this.CLIP_COORDINATE_X1 = null;
      this.CLIP_COORDINATE_X2 = null;
      this.CLIP_COORDINATE_Y1 = null;
      this.CLIP_COORDINATE_Y2 = null;
      this.CLIP_DEST = null;
      this.CLIP_FIELD_NAME = null;
      this.CLIP_QUALITY = null;
      this.CLIP_SLA = null;
      this.CLIP_MANDATORY = null;
      this.CLIP_SEQ_NO = null;
      this.CLIP_SUB_SEQ_NO = null;
      this.CLIP_TYPE = null;
      this.CUSTOM_MESSAGE = null;
      this.LANGUAGE_SRNO = null;
      this.RANGE_VALUE_1 = null;
      this.RANGE_VALUE_2 = null;
    }


    Template.prototype.getMarkedZones = function () {
      return this.markedZoneArray;
    };


    factory.getTemplateInstance = function () {
      var model = new TemplatesModel();
      model.setDefaultMarkedZone();
      return model;
    };

    return factory;
  }


  TemplatesModelFactory.$inject = ['DEFAULT'];

  angular
    .module('app.modules.templates.model', [])
    .factory('templatesModelFactory', TemplatesModelFactory);

})();