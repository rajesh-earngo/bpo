(function () {
  'use strict';

  function TemplatesController(addZoneJqService, templatesJqService, templatesModelFactory, templatesService, alertService) {
    var ctrl = this;
    ctrl.vm = templatesModelFactory.getTemplateInstance();

    ctrl.allowZoneMarking = false;
    ctrl.tmpNumberModal = false;
    ctrl.tmpTextModal = false;
    ctrl.tmpDatetimeModal = false;
    ctrl.tmpPhoneModal = false;
    ctrl.tmpSnsModal = false;

    ctrl.clearFields = function(){
      ctrl.vm.clearMarkedZone();
    }

    ctrl.refCanvas = function(){
      addZoneJqService.loadBoxJQ(ctrl.vm.template.markedZoneArray);
    }

    ctrl.modalType = function(type){
        if(ctrl.vm.allowFieldInput == true){
          switch (type) {
            case 'number':
                ctrl.tmpNumberModal = true; 
                ctrl.tmpTextModal = false; 
                ctrl.tmpMcqModal = false; 
                ctrl.tmpDatetimeModal = false; 
                ctrl.tmpPhoneModal = false; 
                ctrl.tmpSnsModal = false;
                break;
            case 'text':
                ctrl.tmpNumberModal = false; 
                ctrl.tmpTextModal = true; 
                ctrl.tmpMcqModal = false; 
                ctrl.tmpDatetimeModal = false; 
                ctrl.tmpPhoneModal = false; 
                ctrl.tmpSnsModal = false;
                break;
            case 'mcq':
                ctrl.tmpNumberModal = false; 
                ctrl.tmpTextModal = false; 
                ctrl.tmpMcqModal = true; 
                ctrl.tmpDatetimeModal = false; 
                ctrl.tmpPhoneModal = false; 
                ctrl.tmpSnsModal = false;
                break;
            case 'date':
                ctrl.tmpNumberModal = false; 
                ctrl.tmpTextModal = false; 
                ctrl.tmpMcqModal = false; 
                ctrl.tmpDatetimeModal = true; 
                ctrl.tmpPhoneModal = false; 
                ctrl.tmpSnsModal = false;
                break;
            case 'phone':
                ctrl.tmpNumberModal = false; 
                ctrl.tmpTextModal = false; 
                ctrl.tmpMcqModal = false; 
                ctrl.tmpDatetimeModal = false; 
                ctrl.tmpPhoneModal = true; 
                ctrl.tmpSnsModal = false;
                break;
            case 'sns':
                ctrl.tmpNumberModal = false; 
                ctrl.tmpTextModal = false; 
                ctrl.tmpMcqModal = false; 
                ctrl.tmpDatetimeModal = false; 
                ctrl.tmpPhoneModal = false; 
                ctrl.tmpSnsModal = true;
                break;
          }   
          $('#tmp-modal').openModal();         
      }else{
        alertService.alert('Draw the zone first', 'Mandatory');
      }

    }


    ctrl.init = function () {
      templatesJqService.load();
      templatesService.fetchForms(ctrl.vm);
    };


    ctrl.formSelected = function (page) {
      console.log('page : ', page);
      alert('hi');
    };

    ctrl.fetchROI = function (templatePage) {
      templatesService.fetchROI(templatePage, ctrl.vm);
    };

    ctrl.saveROI = function () {
      templatesService.saveROI(ctrl.vm);
    };

  }

  TemplatesController.$inject = ['addZoneJqService', 'templatesJqService', 'templatesModelFactory', 'templatesService', 'alertService'];

  angular
    .module('app.modules.templates', ['app.modules.components.addZone.jq.service', 
      'app.modules.templates.model', 'app.modules.templates.service',
      'app.modules.templates.jq.service', 'app.modules.components.addZone',
      'app.modules.components.selectForm', 'app.modules.components.clips'
    ])
    .controller('templatesController', TemplatesController);
})();