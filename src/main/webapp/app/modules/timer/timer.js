(function () {
    'use strict';


    function TimerDirective($rootScope) {
        var linker = function (scope, element, attrs) {
            var duration = parseInt(element[0].dataset.timer);
            console.log(element[0].dataset.timer)
            var earngoDefaults = {
                animation: 'smooth',
                time: {
                    Days: {
                        show: false
                    },
                    Hours: {
                        show: false
                    },
                    Minutes: {
                        show: false
                    },
                    Seconds: {
                        color: '#FFE74C'
                    }
                },
                total_duration: duration // seconds
            };

            var options = $.extend(true, {}, earngoDefaults, scope.$eval(attrs.timecircles));

            var timer = $(element).TimeCircles(options);

            // destroy the timer when the user starts to navigate to a different page
            // so that the listener callback wouldn't run
            // otherwise, there's an effect where the modal overlay blocks the UI
            // Using $routeChangeSuccess doesn't work cause the initial page load would
            // trigger this event, resulting in the timer being initialized first,
            // then destroyed on route change success
            scope.$on('$routeChangeStart', function (evt, currentPath, prevPath) {
                timer.destroy();
            });

            scope.$on('RESET_TIMER', function (evt) {
                // timeout modal is closed, restart the timer
                timer.restart();
                timer.end().removeClass('glow');
            });

            scope.$on('STOP', function (evt) {
                timer.stop();
            });

            timer.addListener(function (unit, value, total) {
                var $this = $(this); // this = the timer
                if (total === 10) {
                    $this.addClass('glow'); // add glow class to make it blink using css
                }

                if (total === -1) { // when countdown ends (-1)
                    // stop the timer
                    timer.stop();
                    $this.removeClass('glow');

                    // broadcast a signal to open the time out modal
                    $rootScope.$broadcast('OPEN_MODAL');
                    // broadcast a signal to disable submit buttons
                    $rootScope.$broadcast('DISABLE_SUBMIT');
                }
            });

        };

        return {
            restrict: 'E',
            link: linker
        };
    }

    TimerDirective.$inject = ['$rootScope'];

    /** Timer Service */
    function TimerService($rootScope){

        var service ={};

        service.startTimer = function(){
            $rootScope.$broadcast('RESET_TIMER');
        };

        service.stopTimer = function(){
            $rootScope.$broadcast('STOP');
        };

        service.resetTimer = function (){
            // data is valid, reset timer by broadcasting RESET_TIMER event
            $rootScope.$broadcast('RESET_TIMER');
        };

        return service;
    }

    TimerService.$inject = ['$rootScope'];

    angular.module('app.modules.timer', [])
        .directive('egtimer', TimerDirective)
        .service('timerService',TimerService);

})();