(function () {
    'use strict';


    function DashboardController($scope) {

        var ctrl = this;

        ctrl.DoCtrlPagingAct = function(text, page, pageSize, total) {
            console.log(text, page, pageSize, total );
        };

        ctrl.init = function () {
            console.log('Controller dashboard');
            this.total = 100;
        };

        // ctrl.vm = loginModel;

        // ctrl.init = function () {
        //     loginService.deviceInit();
        // };
    }

    DashboardController.$inject = [];

    angular
        .module('app.modules.dashboard', ['bw.paging'])
        .controller('dashboardController', DashboardController);

})();