(function () {
    'use strict';


    function QualityController(qualityService, qualityServiceJQ) {

        var ctrl = this;

/*        ctrl.DoCtrlPagingAct = function(text, page, pageSize, total) {
            console.log(text, page, pageSize, total );
        };*/

        ctrl.onClickForm = function(form){
            this.vm.formSelected = form ;
           // qualityService.formSelected(form);
            qualityService.callImages(this.vm);
        }

        ctrl.selectedImage = function(image){
            ctrl.vm.selectedImageSrc = image.IMG_PATH;
            ctrl.vm.selectedImage = image;
            qualityService.callImagesData(this.vm, ctrl.vm.selectedImage);
        }

        ctrl.postData = function(){
            qualityService.postImagesData(this.vm);
        }

        ctrl.init = function () {
            this.vm = qualityService.initVm();
            //this.pageSize = 3;
            //this.pageNo = 1;
            qualityService.initialize(this.vm);
            qualityServiceJQ.loadJQ();
            //console.log(this.vm.formsArray[1]);
            //this.total = ctrl.vm.formsArraylength;
        };

        // ctrl.paginate = function(){
        //     console.log('in pagi');
        //     //console.log(this.vm.formsArray[1]);
        //     this.begin = ((this.vm.pageNo-1)*this.vm.pageSize);
        //     this.tempBegin = this.begin;
        //     this.end = this.begin + this.vm.pageSize;      
        //     for (var i = 0; i < this.vm.pageSize; i++) {
        //         if( this.tempBegin < this.vm.formsArraylength){
        //             this.vm.formsShowArray[i] = this.vm.formsArray[this.begin+i];
        //             this.tempBegin++;
        //         }
        //     }            
        // }

        // ctrl.vm = loginModel;

        // ctrl.init = function () {
        //     loginService.deviceInit();
        // };
    }

    QualityController.$inject = ['qualityService', 'qualityServiceJQ'];

    angular
        .module('app.modules.quality',['app.modules.quality.service', 'bw.paging', 'app.modules.quality.jq.service'] )
        .controller('qualityController', QualityController);

})();