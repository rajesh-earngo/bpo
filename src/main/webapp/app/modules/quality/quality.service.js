(function () {
    
    'use strict';

    function QualityModelFactory() {

        function QualityModel() {
            this.formsArray = [];
            this.formsArraylength = null;
            this.pageSize = 3;
            this.pageNo = 1;
            this.formsShowArray = [];
            this.imagesArray = [];
            this.imagesDataArray = [];
            this.error = {};
            this.tableShowFlag = true;
            this.selectedImageSrc = null;
            this.customerId = earngoStore.getCustomerId();
        }

        var factory = {};
        factory.getModel = function () {
            return new QualityModel();
        };

        return factory;
    }

    function QualityService(alertService, config, restService, qualityModelFactory) {

        this.initVm = function () {
            return qualityModelFactory.getModel();
        }

        //this.formSelected = function (form) {
        //   console.log(form);
        //};

        this.loadJQ = function () {
            console.log("Load Quality Jquery");
        };

        this.initialize = function (model) {
            var jsonReq = {
                CUST_SRNO: model.customerId,
            }
            restService.httpPostPromise(config.FETCH_QUALITY_FORMS, jsonReq)
                .then(function (result) {
                    if (result.RES_STATUS === 'S') {
                        model.formsArray = result.array;
                        model.formsArraylength = result.array.length;
                    }
                });
        };

        this.callImages = function (model) {
            var jsonReq = {
                IMG_FORM_NUM: model.formSelected.IMG_FORM_NUM
            };
            restService.httpPostPromise(config.FETCH_QUALITY_IMAGES, jsonReq)
                .then(function (result) {
                    model.imagesArray = result;
                    model.selectedImageSrc = result[0].IMG_PATH;
                    model.selectedImage = result[0];
                    var jsonReq2 = {
                        IMAGE_NUM: model.selectedImage.IMG_NUM
                    };
                    restService.httpPostPromise(config.FETCH_QUALITY_IMAGES_DATA, jsonReq)
                        .then(function (result) {
                            model.imagesDataArray = result;
                    });
                    model.tableShowFlag = false;
                });
        };

        this.callImagesData = function (model, selectedImg) {
            var jsonReq = {
                IMAGE_NUM: model.selectedImage.IMG_NUM
            };
            restService.httpPostPromise(config.FETCH_QUALITY_IMAGES_DATA, jsonReq)
                .then(function (result) {
                    model.imagesDataArray = result;
            });
        };

        this.postImagesData = function (model) {
            var jsonReq = {
                CUST_SRNO: model.selectedImage.CUST_SRNO,
                IMG_NUM: model.selectedImage.IMG_NUM,
                CLIP_DATA: model.imagesDataArray
            };
            restService.httpPostPromise(config.FETCH_QUALITY_IMAGES_DATA_POST, jsonReq)
                .then(function (result) {
                    if(result.RES_STATUS === 'S'){
                        console.log(jsonReq);
                        alertService.alert('Changes have been successfully saved !');
                    }else{
                        alertService.error('Changes not saved ! Please Check.');
                    }
            });
        };
        return this;
    }


    QualityService.$inject = ['alertService', 'config', 'restService', 'qualityModelFactory'];

    angular
        .module('app.modules.quality.service', [])
        .factory('qualityModelFactory', QualityModelFactory)
        .service('qualityService', QualityService);

})();