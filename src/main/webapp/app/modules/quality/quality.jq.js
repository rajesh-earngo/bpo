(function () {
    'use strict';

    function QualityServiceJQ() {

        this.loadJQ = function () {
        	console.log('In Quality Jq');
        	$('.qtZoomBtns .fa-plus-circle').click(function() {
        		var w = $('.qtTempImg').width();
        		$('.qtTempImg').width(w + (w/10));
        	});
        	$('.qtZoomBtns .fa-minus-circle').click(function() {
        		var w = $('.qtTempImg').width();
        		$('.qtTempImg').width(w - (w/10));
        	});
        };
        return this;
    }


    QualityServiceJQ.$inject = ['alertService'];

    angular
        .module('app.modules.quality.jq.service', [])
        .service('qualityServiceJQ', QualityServiceJQ);

})();