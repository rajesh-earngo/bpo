https://scotch.io/tutorials/angularjs-best-practices-directory-structure

app/
----- shared   // acts as reusable components or partials of our site
---------- sidebar/
--------------- sidebarDirective.js
--------------- sidebarView.html
---------- article/
--------------- articleDirective.js
--------------- articleView.html
----- modules/   // each module is treated as a mini Angular app
---------- home/
--------------- homeController.js
--------------- homeService.js
--------------- homeView.html
---------- blog/
--------------- blogController.js
--------------- blogService.js
--------------- blogView.html
----- app.module.js
----- app.routes.js
assets/
----- img/      // Images and icons for your app
----- css/      // All styles and style related files (SCSS or LESS files)
----- js/       // JavaScript files written for your app that are not for angular
----- libs/     // Third-party libraries such as jQuery, Moment, Underscore, etc.
index.html




---

ignore .DS_store

Step 1, delete all the *.DS_store files. One can run

git rm -f *.DS_Store
but be aware that rm -f can be a bit dangerous if you have a typo! Step two: add

*.DS_Store
.DS_Store
to .gitignore. This worked for me!
